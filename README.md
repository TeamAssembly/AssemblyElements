# Base Elements for Asynchronous Python Services

#### Installing
Just checkout the code. There's no pip installer yet.


#### Running the tests
The tests expect some pypy and python environments to have been created in advance.

```bash
pypy-3.6.1/bin/virtualenv-pypy venv_pypy_3
source venv_pypy_3/bin/activate
pip install requirements.txt

pypy-2.7.13/bin/virtualenv-pypy venv_pypy_2
source venv_pypy_2/bin/activate
pip install requirements.txt

python-3.7.4/bin/virtualenv venv_python_3
source venv_python_3/bin/activate
pip install requirements.txt

tox
```
