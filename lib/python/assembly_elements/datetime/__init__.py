#!/usr/bin/env python
"""
Additions to datetime to help with timezone-aware datetime objects.

The pyscopg2 driver correctly encodes timezone-aware datetime objects, so we
make every attempt to use them.
"""

from datetime import datetime

from pytz import timezone


UTC = timezone("utc")
EPOCH = datetime(1970, 1, 1, tzinfo=UTC)


def utcnow():
    """Return a timezone-aware datetime object for now."""
    return datetime.now(tz=UTC)


def utcfromtimestamp(timestamp):
    """Return a timezone-aware datetime object for a UNIX timestamp."""
    return datetime.fromtimestamp(timestamp, tz=UTC)
