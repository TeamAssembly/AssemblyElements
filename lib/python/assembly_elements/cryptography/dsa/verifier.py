#!/usr/bin/env python
# Copyright (c) 2018 Donal McMullan
# See LICENSE for details.

"""
Verify a signed message for the user.

Verify the DSA signature supplied for a given message and username, using the
keys in /home/$USERNAME/.ssh
"""

import base64
import os
import platform
import traceback

from ecdsa import BadSignatureError

from twisted import logger

from assembly_elements.util import config

from . import keys



class Verifier(object):
    """Verify a signed message for the user."""

    logger = logger.Logger()
    cache = {}

    @classmethod
    def get_verifier(cls, dsa_keys=None):
        r"""Return a Verifier instance.

        The dsa_keys dictionary if supplied should look something like:
        {
            "base_directory": {
                "linux": "/mnt/keyvolume/keypath/",
                "windows": r"\\fileserver\keyvolume\keypath\"
            },
            "leaf_directory": "dsa"
        }

        That will create a user's key directory like:
        /mnt/keyvolume/keypath/$USERNAME/dsa/

        Args:
            dsa_keys (dict): Configuration information for the keys.

        Returns:
            Verifier: Preconfigured Verifier instance.
        """
        assembly_elements_basedir = os.environ.get("ASSEMBLY_ELEMENTS_BASEDIR", "")

        if dsa_keys is None:
            elements_config_file = os.path.join(
                assembly_elements_basedir,
                "config",
                "assembly_elements.json"
            )
            elements_config_data = config.get(elements_config_file)
            dsa_keys = elements_config_data.get("dsa")["keys"]

        os_family = platform.system().lower()
        dsa_key_base_directory=dsa_keys["base_directory"][os_family]
        dsa_key_leaf_directory=dsa_keys["leaf_directory"]

        return cls(
            base_directory=dsa_key_base_directory,
            leaf_directory=dsa_key_leaf_directory
        )

    def __init__(self, **kwargs):
        """Initialize an instance of Signer.

        Check the keys.py file for the latest keyword arguments for KeyFileHandler.
        Currently it accepts:
            base_directory
            leaf_directory
            verifying_key_filename
            signing_key_filename

        Args:
            kwargs (dict): Pass through these args to KeyFileHandler.
        """
        self.cache = {}
        self.keyfile_handler = None
        self.keyfile_handler = keys.KeyFileHandler(**kwargs)

    def get_verifying_key(self, username):
        """Return the verifying key for 'username'.

        Args:
            username (str): A username.

        Returns:
            ecdsa.VerifyingKey: Verifying key instance.
        """
        if username not in self.cache:
            self.refresh_key(username)
        return self.cache[username]

    def refresh_key(self, username):
        """Refresh the verifying key data for 'username' from the filesystem.

        Args:
            username (str): A username.

        Raises:
            Exception: If key retrieval fails.

        Returns:
            ecdsa.VerifyingKey: Verifying key instance.
        """

        try:
            verifying_key = self.keyfile_handler.get_verifying_key(username)
            self.cache[username] = verifying_key
            return verifying_key
        except Exception:
            self.logger.error(traceback.format_exc())
            raise

    def verify_signature(self, username, message, signature, attempt=0):
        """Verify 'username's signature of a message.

        If verification fails and attempt is zero, the code will try to refresh the
        user's keys, and re-verify with the fresh key.

        Args:
            username (str): A username.
            message (str|bytes): A message we need to verify.
            signature (bytes): The message signature to verify.
            attempt (int): How many attempts we have made.

        Raises:
            BadSignatureError: If signature verification fails.
            RuntimeError: If urlsafe_b64decode fails.
            Exception: If verification throws an exception.

        Returns:
            bool: True if the signature is correct.
        """
        try:
            decoded_signature = base64.urlsafe_b64decode(signature)
        except Exception as e:
            msg = "bad signature: '{0}': {1}".format(signature, e)
            self.logger.error(msg)
            raise RuntimeError(msg)

        if isinstance(message, bytes):
            message_bytes = message
        else:
            message_bytes = message.encode("utf-8")

        verifier = self.get_verifying_key(username)

        try:
            verifier.verify(decoded_signature, message_bytes)
            return True
        except BadSignatureError as e:
            msg = "bad signature for user '{username}': {exception}"
            self.logger.error(msg, username=username, exception=e)
            self.logger.debug(traceback.format_exc())
            """
            If the signature looks bad, refresh the user's keys and retry once
            before raising an Exception
            """
            if attempt == 0:
                self.refresh_key(username)
                return self.verify_signature(username, message, signature, attempt=1)
            raise
        except Exception as e:
            """
            This code should be unreachable to we log the traceback to try to work out
            what could have gone wrong.
            """
            msg = (
                "unexpected exception on signature verification "
                "for user '{username}': {exception}"
            )
            self.logger.error(msg, username=username, exception=e)
            self.logger.error(traceback.format_exc())
            raise
