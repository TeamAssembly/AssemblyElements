#!/usr/bin/env python
# Copyright (c) 2018 Donal McMullan
# See LICENSE for details.

"""ECDSA Key management."""

import errno
import os
try:
    import pwd
except ImportError:
    pwd = None

import stat
import traceback
from collections import namedtuple

from assembly_elements.util.exceptions import AssemblyEnvironmentError
from assembly_elements.util.exceptions import AssemblyRuntimeError

import ecdsa

from twisted import logger

try:
    from builtins import FileExistsError
except Exception:
    FileExistsError = OSError
    FileNotFoundError = IOError


class KeyFileHandler(object):
    """ECDSA Key management."""

    logger = logger.Logger()
    KeyPair = namedtuple("KeyPair", ("signing_key", "verifying_key"))

    # Filesystem permissions from octal to decimal
    OCT400 = int("400", 8)  # 256
    OCT500 = int("500", 8)  # 320
    OCT600 = int("600", 8)  # 384
    OCT644 = int("644", 8)  # 420
    OCT700 = int("700", 8)  # 448
    OCT750 = int("750", 8)  # 488
    OCT755 = int("755", 8)  # 493

    default_verifying_key_filename = "assembly_public.pem"
    default_signing_key_filename = "assembly_private.pem"

    default_base_directory = None
    default_leaf_directory = ".ssh"

    def __init__(
        self,
        base_directory=None,
        leaf_directory=None,
        verifying_key_filename=None,
        signing_key_filename=None,
    ):
        """Constructor."""
        self.base_directory = base_directory or self.default_base_directory
        self.leaf_directory = leaf_directory or self.default_leaf_directory

        self.verifying_key_filename = (
            verifying_key_filename or self.default_verifying_key_filename
        )
        self.signing_key_filename = (
            signing_key_filename or self.default_signing_key_filename
        )

    def path_join(self, *args):
        args = list(args)
        for index, element in enumerate(args):
            if isinstance(element, bytes):
                args[index] = element.decode("utf-8")
        return os.path.join(*args)

    def create_keypair(self, username):
        """Create public and private ecdsa keys for user 'username'."""
        signing_key = ecdsa.SigningKey.generate()
        verifying_key = signing_key.get_verifying_key()

        signing_key_path = self.get_keypath(username, self.signing_key_filename)
        verifying_key_path = self.get_keypath(username, self.verifying_key_filename)

        # Create keyfiles and set their permissions correctly:
        keydata = signing_key.to_pem()
        self.create_keyfile(signing_key_path, self.OCT600, username, keydata)

        keydata = verifying_key.to_pem()
        self.create_keyfile(verifying_key_path, self.OCT644, username, keydata)

        return self.KeyPair(signing_key, verifying_key)

    def create_keyfile(self, keypath, permissions, username, keydata=None):
        """
        Create the key file.

        We *must* restrict its read permissions *before* we write the data
        to it.
        """
        # Create the empty key file
        try:
            fh = open(keypath, "w").close()
        except Exception:
            self.logger.error(traceback.format_exc())
            raise

        if isinstance(username, bytes):
            username = username.decode("utf-8")

        # In case the key already existed, make sure that it's utime is fresh
        try:
            os.utime(keypath, None)
        except Exception:
            # ~impossible to trigger this exception if open() worked above?
            self.logger.error(traceback.format_exc())
            raise

        # Fix the ownership of the file, if it is not already correct
        try:
            details = pwd.getpwnam(username)
            target_uid = details.pw_uid
            target_gid = details.pw_gid
            current_uid = os.getuid()
            current_gid = os.getgid()
            if target_uid != current_uid and target_gid != current_gid:
                os.chown(keypath, target_uid, target_gid)
        except Exception:
            self.logger.error(traceback.format_exc())
            raise

        # Set the permissions on the key file
        try:
            os.chmod(keypath, permissions)
        except Exception:
            self.logger.error(traceback.format_exc())
            raise

        if keydata is None:
            return

        try:
            fh = open(keypath, "wb")
            fh.write(keydata)
            fh.close()
        except Exception:
            msg = "failed to write key data to path '{keypath}'"
            self.logger.error(msg, traceback=traceback.format_exc(), keypath=keypath)
            raise

        """
        The file now owned by root again, so we have to chown
        """
        try:
            os.chown(keypath, target_uid, target_gid)
        except Exception:
            msg = "failed to chown '{keypath}'"
            self.logger.error(msg, traceback=traceback.format_exc(), keypath=keypath)
            raise

        try:
            st = os.stat(keypath)
            self.logger.error("stat keyfile: {}".format(username), keypath=keypath, uid=st.st_uid, gid=st.st_gid)
        except Exception:
            msg = "failed to stat path '{keypath}'"
            self.logger.error(msg, traceback=traceback.format_exc(), keypath=keypath)
            raise

    def get_or_create_signing_key(self, username):
        """Return the user's signing key, creating it if necessary."""
        try:
            return self.get_signing_key(username)
        except FileNotFoundError as e:
            if e.errno != errno.ENOENT:
                raise

            keypair = self.create_keypair(username)
            return keypair.signing_key

    def get_or_create_verifying_key(self, username):
        """Return the user's verifying key, creating it if necessary."""
        try:
            return self.get_verifying_key(username)
        except FileNotFoundError as e:
            if e.errno != errno.ENOENT:
                raise

            keypair = self.create_keypair(username)
            return keypair.verifying_key

    def get_signing_key(self, username):
        """Return the user's signing key."""
        keydata = self.get_keydata(username, self.signing_key_filename)
        signing_key = ecdsa.SigningKey.from_pem(keydata)
        return signing_key

    def get_verifying_key(self, username):
        """Return the user's verifying key."""
        keydata = self.get_keydata(username, self.verifying_key_filename)
        verifying_key = ecdsa.VerifyingKey.from_pem(keydata)
        return verifying_key

    def get_keydata(self, username, filename):
        """Return the contents of file filename for user 'username'."""
        keypath = self.get_keypath(username, filename)
        with open(keypath, "rb") as fh:
            keydata = fh.read()

        return keydata

    def check_directory_permissions(self, directory, username, permissions):
        """Check that directory is owned by username and perms are 0700."""
        metadata = os.stat(directory)
        directory_mode = stat.S_IMODE(metadata.st_mode)
        if directory_mode != permissions:
            msg = "permissions on {0} are incorrect: expected: {1}  got: {2}".format(
                directory, oct(permissions), oct(directory_mode)
            )
            raise AssemblyEnvironmentError(msg)

        """Check that directory is owned by the 'username' user."""
        userdata = pwd.getpwnam(username)
        user_id = userdata.pw_uid
        if metadata.st_uid != user_id:
            owner = pwd.getpwuid(metadata.st_uid).pw_name
            msg = "directory exists but is owned by '{0}'".format(owner)
            raise AssemblyRuntimeError(msg)

        return directory

    def check_keyfile_directory_permissions(self, keyfile_directory, username):
        """Check that directory is owned by username and perms are 0700."""
        keyfile_directory_permissions = self.OCT700
        return self.check_directory_permissions(
            keyfile_directory, username, keyfile_directory_permissions
        )

    def check_user_directory_permissions(self, user_directory, username):
        """Check that directory is owned by username and perms are 0750."""
        user_directory_permissions = self.OCT750
        return self.check_directory_permissions(
            user_directory, username, user_directory_permissions
        )

    def create_user_directory(self, username):
        """Create the base directory for the user."""
        user_directory = self.path_join(self.base_directory, username)
        return self.create_directory(user_directory, username, self.OCT750)

    def create_directory(self, directory, username, permissions):
        """
        Create the leaf directory for a user.

        If the directory already exists, just confirm that its permissions and
        ownership are correct.
        """
        if isinstance(username, bytes):
            username = username.decode("utf-8")

        if os.path.isdir(directory):
            return self.check_directory_permissions(directory, username, permissions)

        try:
            os.mkdir(directory, permissions)
        except FileExistsError as e:
            # if the directory-create failed because it already exists, we
            # ignore the error
            if e.errno == errno.EEXIST and os.path.isdir(directory):
                # We can only get here on a race condition
                return self.check_directory_permissions(
                    directory, username, permissions
                )
            raise
        except Exception:
            msg = "failed to create leaf directory at '{directory}'"
            self.logger.error(
                msg, traceback=traceback.format_exc(), directory=directory
            )
            raise

        try:
            userdata = pwd.getpwnam(username)
            user_id = userdata.pw_uid
            os.chown(directory, user_id, -1)
        except Exception:
            msg = "failed to chown directory at '{directory}'"
            self.logger.error(
                msg, traceback=traceback.format_exc(), directory=directory
            )
            raise

        try:
            os.chmod(directory, permissions)
        except Exception:
            msg = "failed to chmod directory at '{directory}'"
            self.logger.error(
                msg, traceback=traceback.format_exc(), directory=directory
            )
            raise

        return directory

    def create_keyfile_directory(self, username):
        """Create the keyfile directory for the user."""
        keyfile_directory = self.path_join(
            self.base_directory, username, self.leaf_directory
        )
        return self.create_directory(keyfile_directory, username, self.OCT700)

    def get_keypath(self, username, filename):
        """Get the path to 'filename' for user 'username'."""
        keyfile_directory = self.get_keyfile_directory(username)
        if not os.path.exists(keyfile_directory):
            self.create_user_directory(username)
            self.create_keyfile_directory(username)
        return self.path_join(keyfile_directory, filename)

    def get_keyfile_directory(self, username):
        """Get the path to the leaf directory for user 'username'."""
        if self.base_directory is not None:
            return self.path_join(self.base_directory, username, self.leaf_directory)

        home_directory = os.path.expanduser("~{0}".format(username))
        return self.path_join(home_directory, self.leaf_directory)
