#!/usr/bin/env python
# Copyright (c) 2018 Donal McMullan
# See LICENSE for details.

"""
Sign 'message. to prove it was issued by 'username'.

Generate the DSA signature for a given message and username, using the private
key stored in /home/$USERNAME/.ssh
"""

import base64
import os
import platform

from twisted import logger

from assembly_elements.util import config

from . import keys


class Signer(object):
    """Sign 'message. to prove it was issued by 'username'."""

    logger = logger.Logger()

    @classmethod
    def get_signer(cls, dsa_keys=None):
        r"""Return a Signer instance.

        The dsa_keys dictionary if supplied should look something like:
        {
            "base_directory": {
                "linux": "/mnt/keyvolume/keypath/",
                "windows": r"\\fileserver\keyvolume\keypath\"
            },
            "leaf_directory": "dsa"
        }

        That will create a user's key directory like:
        /mnt/keyvolume/keypath/$USERNAME/dsa/

        Args:
            dsa_keys (dict): Configuration information for the keys.

        Returns:
            Signer: Preconfigured Signer instance.
        """
        assembly_elements_basedir = os.environ.get("ASSEMBLY_ELEMENTS_BASEDIR", "")

        if dsa_keys is None:
            elements_config_file = os.path.join(
                assembly_elements_basedir,
                "config",
                "assembly_elements.json"
            )
            elements_config_data = config.get(elements_config_file)
            dsa_keys = elements_config_data.get("dsa")["keys"]

        os_family = platform.system().lower()
        dsa_key_base_directory = dsa_keys["base_directory"][os_family]
        dsa_key_leaf_directory = dsa_keys["leaf_directory"]

        return cls(
            base_directory=dsa_key_base_directory,
            leaf_directory=dsa_key_leaf_directory
        )

    def __init__(self, **kwargs):
        """Initialize an instance of Signer.

        Check the keys.py file for the latest keyword arguments for KeyFileHandler.
        Currently it accepts:
            base_directory
            leaf_directory
            verifying_key_filename
            signing_key_filename

        Args:
            kwargs (dict): Pass through these args to KeyFileHandler.
        """
        self.cache = {}
        self.keyfile_handler = None
        self.keyfile_handler = keys.KeyFileHandler(**kwargs)

    def get_signing_key(self, username):
        """Return the signing key for 'username'.

        Args:
            username (str): A username.

        Returns:
            ecdsa.SigningKey: Signing key instance.
        """
        if username not in self.cache:
            self.refresh_key(username)
        return self.cache[username]

    def refresh_key(self, username):
        """Refresh the signing key data for 'username' from the filesystem.

        Args:
            username (str): A username.

        Raises:
            Exception: If key retrieval fails.

        Returns:
            ecdsa.SigningKey: Signing key instance.
        """
        try:
            signing_key = self.keyfile_handler.get_or_create_signing_key(username)
            self.cache[username] = signing_key
            return signing_key
        except Exception:
            self.logger.error(
                "Failed to get signing key for {username}", username=username
            )
            raise

    def sign_message(self, username, message):
        """Return 'username's unique signature for 'message'.

        Args:
            username (str): A username.
            message (str|bytes): A message we need to sign.

        Raises:
            Exception: If signing fails.

        Returns:
            bytes: The signature.
        """
        try:
            signing_key = self.get_signing_key(username)

            if isinstance(message, bytes):
                message_bytes = message
            else:
                message_bytes = message.encode("utf-8")
            signature = signing_key.sign(message_bytes)
            return base64.urlsafe_b64encode(signature)
        except Exception:
            self.logger.error(
                "Failed to sign message for {username}", username=username
            )
            raise
