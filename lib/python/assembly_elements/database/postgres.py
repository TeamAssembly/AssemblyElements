#!/usr/bin/env python
"""Postgres Pool."""

from assembly_elements.util import exceptions as assembly_exceptions

# The psycopg2 imports must precede the txpostgres imports
from .psycopg2 import extras  # noqa
from .psycopg2 import psycopg2  # noqa

from twisted import logger  # noqa
from twisted.internet import defer  # noqa

from txpostgres import txpostgres
from txpostgres.reconnection import DeadConnectionDetector


class PatchedConnection(txpostgres.Connection):
    """Postgres Connection for Async."""

    def connect(self, *args, **kwargs):
        """
        Connect to the database.

        The reason this class exists is because the psycopg2cffi expects the
        keyword argument 'async_' whereas txpostgres passes in 'async'.

        Any arguments will be passed to :attr:`connectionFactory`. Use them to
        pass database names, usernames, passwords, etc.

        :return: A :d:`Deferred` that will fire when the connection is open.

        :raise: :exc:`~txpostgres.txpostgres.AlreadyConnected` when the
            connection has already been opened.
        """
        if self.detector:
            self.detector.setReconnectable(self, *args, **kwargs)

        if self._connection and not self._connection.closed:
            return defer.fail(txpostgres.AlreadyConnected())

        kwargs["async_"] = True
        try:
            self._connection = self.connectionFactory(*args, **kwargs)
        except Exception:
            return defer.fail()

        def startReadingAndPassthrough(ret):
            """Add socket to reactor."""
            self.reactor.addReader(self)
            return ret

        # The connection is always a reader in the reactor, to receive NOTIFY
        # events immediately when they're available.
        d = self.poll()
        return d.addCallback(startReadingAndPassthrough)


class Pool(txpostgres.ConnectionPool):
    """Postgres Connection Pool."""

    instances = {}
    ready = False
    logger = logger.Logger()

    @classmethod
    def initialize(cls):
        """Initialize psycopg2 extensions."""
        if cls.ready:
            return
        psycopg2.extensions.register_type(psycopg2.extensions.UNICODE)
        psycopg2.extensions.register_type(psycopg2.extensions.UNICODEARRAY)
        cls.ready = True

    def purge_cache(self):
        """Close all connections and clear the connection cache."""
        instances = self.__class__.instances
        for key, instance in instances.items():
            try:
                df = instance.close()
                df.addErrback(lambda x: None)
            except Exception:
                pass
        instances.clear()

    @classmethod
    def get_instance_from_config(
        cls, config_object, min_connections=None, max_connections=None
    ):
        """Get a Pool instance for a database that is defined in config."""
        if not cls.ready:
            cls.initialize()

        if config_object not in cls.instances:
            cls.instances[config_object] = cls(
                config_object,
                min_connections=min_connections,
                max_connections=max_connections,
            )
        return cls.instances[config_object]

    def _log_failure(self, f):
        """Log (and re-raise) failures."""
        self.logger.failure("Postgres pool error", f)
        return f

    def connectionFactory(self, *args, **kwargs):  # noqa
        """
        Return a Postgres connection.

        This method is required by txpostgres. Don't call it directly.
        """
        kwargs["detector"] = DeadConnectionDetector()
        return PatchedConnection(*args, **kwargs)

    def addConnection(self):  # noqa
        """
        Add a connection to the pool.

        Note that this returns None - not a connection object.
        """
        if len(self.connections) >= self.max_connections:
            raise assembly_exceptions.AssemblyRuntimeError(
                "max connections reached - cannot addConnection"
            )

        conn = self.connectionFactory(self.reactor, self.cooperator)
        conn_df = conn.connect(*self.connargs, **self.connkw)
        conn_df.addCallback(lambda x: self.add(conn))
        conn_df.addCallback(lambda x: None)
        conn_df.addErrback(self._log_failure)
        return conn_df

    def __init__(self, config_object, min_connections=None, max_connections=None):
        """
        Constructor.

        This raises an exception if called directly.

        Don't instance this class directly - you must call the get_instance
        method.
        """
        self._status_df = None
        self._pending_start = []

        database_config = config_object.get("credentials")

        self.min_connections = min_connections or config_object.get(
            "min_connections", 8
        )
        self.max_connections = max_connections or config_object.get(
            "max_connections", 256
        )

        txpostgres.ConnectionPool.__init__(
            self,
            None,
            connection_factory=extras.NamedTupleConnection,
            min=self.min_connections,
            **database_config
        )

    def start(self):
        """
        Start the connection pool.

        This returns a deferred that fires when all the connections are up and
        the pool is ready to run queries.
        """
        if self._status_df is None:
            self._status_df = txpostgres.ConnectionPool.start(self)
            self._status_df.addCallback(self._start_succeeded)
            self._status_df.addErrback(self._start_failed)
        elif self._status_df.called:
            return defer.succeed(self._status_df.result)

        """
        Every call to start gets a deferred that will be callback'ed with the
        result of self._status_df
        """
        df = defer.Deferred()
        self._pending_start.append(df)
        return df

    def close(self):
        """
        Close all the connections in the pool.

        Also - reset the status_df in case our 'start' method gets called again
        after our close method
        """
        self._status_df = None
        return txpostgres.ConnectionPool.close(self)

    def _start_succeeded(self, result):
        """Respond to successful connections to Postgres."""
        dependents = self._pending_start[:]
        self._pending_start = []

        for dependent in dependents:
            dependent.callback(result)

    def _start_failed(self, fail):
        """Respond to a failure to establish our connections."""
        self.logger.error(
            "failed to connect {traceback}", traceback=fail.getBriefTraceback()
        )

        dependents = self._pending_start[:]
        self._pending_start = []

        for dependent in dependents:
            try:
                dependent.errback(fail)
            except Exception as e:
                self.logger.error(
                    "failed to trigger dependent errback: {exception}", exception=str(e)
                )
        return fail
