#!/usr/bin/env python
"""Choose the correct postgres driver for your interpreter."""

from __future__ import absolute_import

import platform

if "PyPy" == platform.python_implementation():
    from psycopg2cffi import compat

    compat.register()

from psycopg2 import extensions  # noqa

extensions.register_type(extensions.UNICODE)
extensions.register_type(extensions.UNICODEARRAY)
from psycopg2 import extras  # noqa
import psycopg2  # noqa
