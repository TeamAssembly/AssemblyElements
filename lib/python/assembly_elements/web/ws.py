#!/usr/bin/env python

import time

from autobahn.twisted import websocket

from assembly_elements.util import jsondate
from assembly_elements.util import exceptions

from twisted import logger
from twisted.internet import defer
from twisted.internet import reactor
from twisted.internet import task


from .resource import AssemblyWebSocketResource
from .auth import AssemblyEcdsaAuth
from .auth import AssemblyLdapAuth
from assembly_elements.user import User


class AssemblyWebSocketServerFactory(websocket.WebSocketServerFactory):

    logger = logger.Logger()

    def __init__(self, *args, **kwargs):
        self.authentication_required = kwargs.pop("authentication_required", False)
        websocket.WebSocketServerFactory.__init__(self, *args, **kwargs)

        self.setProtocolOptions(autoPingInterval=10, autoPingTimeout=10)
        self._protocols = set()
        self.timeout_loop = None
        self.beginLooping()
        self.registry = {
            "ecdsa_auth.request_token":AssemblyEcdsaAuth.request_token,
            "ecdsa_auth.authenticate":AssemblyEcdsaAuth.authenticate,
            "ldap_auth.authenticate":AssemblyLdapAuth.authenticate
        }

    def connectionLost(self, connection):
        self.logger.debug("AssemblyWebSocketServerFactory.connectionLost")
        try:
            self._protocols.remove(connection)
        except Exception:
            self.logger.error(
                "ERROR: Tried to remove connection from factory it doesn't belong to."
            )

    def buildProtocol(self, addr):
        self.logger.debug("AssemblyWebSocketServerFactory.buildProtocol")
        protocol = websocket.WebSocketServerFactory.buildProtocol(self, addr)
        self._protocols.add(protocol)
        return protocol

    def beginLooping(self):
        if self.timeout_loop is not None:
            return

        if hasattr(self.protocol, 'timeoutOldRequests'):
            self.timeout_loop = task.LoopingCall(self.timeoutOldRequests)
            self.timeout_loop.start(10, False)

    def timeoutOldRequests(self):
        for protocol in self._protocols:
            protocol.timeoutOldRequests()

    def testResponseMethod(self, *args, **kwargs):
        """Return a success result to the client for testing."""
        self.logger.debug("testResponseMethod")
        return {"args": args, "kwargs": kwargs, "extras": [1, 2, 3]}

    def testErrorResponseMethod(self, *args, **kwargs):
        """Return an error result to the client for testing."""
        self.logger.debug("testErrorResponseMethod")
        rval = "an error occurred and your request could not be completed"
        raise exceptions.AssemblyWebSocketException(exceptions.AssemblyWebSocketException.unexpected_error, rval)

    def registerMethod(self, name, method):
        """Register a method name and a method as available for remote execution."""
        self.logger.debug("registerMethod: {} :: {}".format(name, method))
        if name in self.registry:
            raise exceptions.AssemblyException(
                "method name '{}' is already represented in "
                "AssemblyWebSocketServerFactory.registry by '{}'".format(
                    name, self.registry[name]
                )
            )
        self.registry[name] = method


class AssemblyWebSocketServerProtocol(websocket.WebSocketServerProtocol):

    notification_type = -2
    request_type = -1
    response_type = 0
    error_type = 1

    logger = logger.Logger()

    def __init__(self):
        """Constructor."""
        self.logger.debug("Constructor")
        websocket.WebSocketServerProtocol.__init__(self)
        self.user = None
        self.client_id = None
        self.request_id = 0
        self.request_hash = {}
        self.session = None

        self.testInitiateRequestToClient()

    def timeoutOldRequests(self):
        """
        Every ten seconds our ProtocolFactory calls this method to sweep expired requests.
        """
        now = time.time()
        for key in self.request_hash.keys():
            timeout, df = self.request_hash[key]
            if timeout > now:
                continue
            self.request_hash.pop(key)
            try:
                df.errback("timeout")
            except Exception as e:
                self.logger.error("ERROR: Failed to errback on timeout: {}".format(e))

    def testInitiateRequestToClient(self, *args, **kwargs):
        """Return a success result to the client for testing."""
        self.logger.debug("testInitiateRequestToClient")

        client_method = "alert_user"
        client_args = ["Here is a message from the server: {}".format(str(self.session))]
        client_kwargs = {}
        timeout = 10

        df = task.deferLater(
            reactor,
            2,
            self.sendNotification,
            client_method,
            client_args,
            client_kwargs,
            timeout
        )
        df.addErrback(self.log_error, msg="client side error")

    def log_error(self, f, msg="UNEXPECTED ERROR", trap=False):
        self.logger.error(msg, traceback=f.getBriefTraceback())
        if not trap:
            return f

    def authenticate(self, user, signature, payload):
        self.logger.debug("authenticate")
        method = self.factory.registry["authenticate"]
        username = method(user, signature, payload)
        if not username:
            return False

        self.logger.debug("authenticated user", username=username)
        self.setUser(User(username))
        #parameters = self.session.factory.get_session_parameters()
        df = self.session.authenticate(username, self.session.attributes)
        df.addCallback(lambda x:True)
        return df

    def updateSessionCookie(self, name, value):
        pass

    def callRequestedMethod(self, name, *args, **kwargs):
        self.logger.debug("callRequestedMethod", method=name)
        if not name in self.factory.registry:
            rval = "method '{}' does not exist or has not been exported"
            raise exceptions.AssemblyWebSocketException(exceptions.AssemblyWebSocketException.no_such_method, rval)

        method = self.factory.registry[name]
        return defer.maybeDeferred(method, self.user, *args, **kwargs)

    def setSessionObject(self, session):
        """Add a session object. This is called in AssemblyWebSocketResource."""
        self.logger.debug("setSessionObject")
        self.session = session

    def setUser(self, user=None):
        """Set the authenticated user for this protocol."""
        self.user = user

    def setClientId(self, client_id=None):
        """Set the client_id for this protocol."""
        self.client_id = client_id

    def onConnect(self, request):
        """Respond to payload."""
        websocket.WebSocketServerProtocol.onConnect(self, request)
        self.logger.debug("onConnect")

    def connectionLost(self, reason=None):
        try:
            reason = str(reason.value)
        except Exception:
            reason = str(reason)
        self.logger.debug("connectionLost", reason=reason)
        self.factory.connectionLost(self)
        return websocket.WebSocketServerProtocol.connectionLost(self, reason)

    def onMessage(self, payload, isBinary):
        """Respond to payload."""
        self.logger.debug("onMessage", payload=str(payload), isBinary=isBinary)
        message = jsondate.loads(payload)
        if message[0] == self.notification_type:
            self.processRequest(message)
        if message[0] == self.request_type:
            self.processRequest(message, reply=True)
        elif message[0] == self.response_type:
            self.processResponse(message)
        else:
            self.processResponse(message)

    def processResponse(self, message):
        """Handle an incoming RPC response."""
        self.logger.debug("processResponse")
        try:
            message_type, request_id, result = message
        except Exception as e:
            self.logger.error("no unpack", e=str(e))
            pass

        try:
            self.logger.error(
                "request_id",
                request_id=request_id,
                keys=self.request_hash.keys()
            )
            timeout, df = self.request_hash.pop(request_id)
            del timeout
        except Exception as e:
            self.logger.error("no pop", e=str(e))
            raise

        if not df:
            self.logger.error(
                "ERROR: No deferred was found for incoming response",
                request_id=request_id
            )
            return

        self.logger.debug("processResponse 2")
        try:
            if message_type == self.response_type:
                df.callback(result)
            else:
                df.errback(result)
        except Exception as e:
            self.logger.error(
                "ERROR: Failed to callback with result for request: {}".format(e)
            )
        self.logger.debug("processResponse 3")

    def processRequest(self, message, reply=False):
        """Respond to an incoming RPC request."""
        self.logger.debug("processRequest:")
        try:
            _, request_id, method, args, kwargs = message
        except Exception as e:
            self.logger.error("ex: {}".format(e), e=e)
            raise

        self.logger.debug(
            "request_id/method", request_id=request_id, method=str(method)
        )
        try:
            df = self.callRequestedMethod(method, *args, **kwargs)
            if reply:
                df.addCallback(self.sendResponse, request_id)
                df.addErrback(self.sendErrorResponse, request_id)
                df.addErrback(self.sendUncaughtErrorResponse, request_id)
                df.addErrback(self.log_error, "failed to process request")
            else:
                df.addErrback(self.log_error, "failed to process notification", trap=True)
        except Exception as e:
            self.logger.error("ex: {}".format(e), e=e)

    def sendResponse(self, result, request_id):
        """Send a response back to the client."""
        self.logger.debug("sendResponse: {} :: {}".format(request_id, result))
        response = (self.response_type, request_id, result)
        self.sendObject(response)

    def sendErrorResponse(self, failure, request_id):
        """Respond to an error."""
        self.logger.debug("sendErrorResponse: {} :: {}".format(request_id, failure))
        failure.trap(exceptions.AssemblyWebSocketException)
        ex = failure.value
        response = (self.error_type, request_id, (ex.errno, ex.message))
        self.sendObject(response)

    def sendUncaughtErrorResponse(self, failure, request_id):
        """
        Respond to an uncaught error.

        We discard the 'failure' object, because we *never* share any information
        about the internals of the application with the peer.
        """
        self.logger.error(
            "sendUncaughtErrorResponse: {} :: {}".format(request_id, failure)
        )
        error_number = 1
        error_message = "unexpected error"
        response = (self.error_type, request_id, (error_number, error_message))
        self.sendObject(response)

    def sendRequest(
        self, method, args_list, kwargs_dict, timeout=10
    ):
        """Send an RPC request to the peer."""
        return self.sendRequestOrNotification(self.request_type, method, args_list, kwargs_dict, timeout)

    def sendNotification(
        self, method, args_list, kwargs_dict, timeout=10
    ):
        """Send an RPC notification to the peer."""
        return self.sendRequestOrNotification(self.notification_type, method, args_list, kwargs_dict, timeout)

    def sendRequestOrNotification(
        self, message_type, method, args_list, kwargs_dict, timeout=10
    ):
        """Send an RPC request or notification to the peer."""
        self.logger.debug(
            "sendRequestOrNotification: {} :: {} :: {}".format(method, args_list, kwargs_dict)
        )

        request_id = self.request_id
        self.request_id += 1

        df = None
        if message_type == self.request_type:
            df = defer.Deferred()
            timeout = time.time() + timeout
            self.request_hash[request_id] = (timeout, df)

        request = [message_type, request_id, method, args_list, kwargs_dict]
        self.sendMessage(jsondate.dumps(request).encode("utf-8"), isBinary=False)
        return df

    def sendObject(self, obj):
        """Encode an object and send it to the peer."""
        return self.sendMessage(jsondate.dumps(obj).encode("utf-8"), isBinary=False)


def AssemblyWebSocketServerResource():
    websocket_factory = AssemblyWebSocketServerFactory(authentication_required=True)

    websocket_factory.registerMethod(
        "test_response_method", websocket_factory.testResponseMethod
    )
    websocket_factory.registerMethod(
        "test_error_response_method", websocket_factory.testErrorResponseMethod
    )
    websocket_factory.protocol = AssemblyWebSocketServerProtocol
    websocket_factory.startFactory()  # when wrapped as a Twisted Web resource, start the underlying factory manually
    websocket_resource = AssemblyWebSocketResource(websocket_factory)
    return websocket_resource
