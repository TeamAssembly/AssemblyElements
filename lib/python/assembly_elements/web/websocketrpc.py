#!/usr/bin/env python
"""RPC endpoint over Websockets."""

from __future__ import absolute_import

try:
    from http import HTTPStatus
except ImportError:
    from httpstatus import HTTPStatus
import json

from assembly_elements.cryptography import dsa

from autobahn.twisted import resource
from autobahn.twisted import websocket

import six

from twisted import logger
from twisted.internet import defer
from twisted.internet import reactor
from twisted.protocols.policies import ProtocolWrapper
from twisted.web import server
from twisted.web.resource import IResource

from zope.interface import implementer

from .resource import AssemblyWebSocketResource as WebSocketRPCResource


class WebSocketRPCProtocol(websocket.WebSocketServerProtocol):
    """Twisted websocket protocol."""

    logger = logger.Logger()

    def __init__(self, *args, **kwargs):
        """Constructor."""
        websocket.WebSocketServerProtocol.__init__(self)
        self.ready = defer.Deferred()
        self.session = None

    def _check_session(self, session, request):
        self.logger.error("_check_session: {}".format(session.session_id))
        self.ready.callback(session)

    def setSessionObject(self, session):
        self.session = session

    def onConnect(self, request):
        """Respond to a new inbound connection."""
        self.logger.error("websocket connected: {}".format(type(request).__name__))
        self.logger.error("websocket connected: {}".format("request"))
        self.logger.error("websocket connected: {}".format(dir(request.protocols)))
        self.logger.error("headers: {}".format(request.headers.get("cookie")))

        response = json.dumps({"message":"a-ok", "session_id":self.session.session_id.decode("utf-8")}, indent=4).encode("utf-8")
        reactor.callLater(2, self.sendMessage, response, False)

    def log_error(self, f):
        """Log the traceback for any failure. Don't trap."""
        traceback = f.getBriefTraceback()
        self.logger.error("unexpected error: {}".format(traceback))
        return f

    def onMessage(self, payload, isBinary):
        """Respond to a new message, received from the client."""
        self.logger.error("WebSocketRPCProtocol.onMessage")
        self.logger.error("WebSocketRPCProtocol.onMessage", session_id=self.session.session_id)

        try:
            message = json.loads(payload)
            message.append({"extra_info": 1000})
            payload = json.dumps(message).format("utf-8")
        except Exception as e:
            self.logger.error("error", e=str(e))

        try:
            self.ready.addCallback(self._onMessage, payload, isBinary)
            if isBinary:
                self.logger.debug("Binary message received: {0} bytes".format(len(payload)))
            else:
                self.logger.debug(
                    "Text message received: {0}".format(payload.decode("utf8"))
                )

            if isinstance(payload, six.text_type):
                payload = payload.encode("utf-8")

            # echo back message verbatim
            self.sendMessage(json.dumps({"name":"donal","session_id": self.session.session_id.decode("utf-8")}).encode("utf-8"), False)
            self.sendMessage(payload, isBinary)
        except Exception as e:
            self.logger.error("error", e=str(e))
        else:
            self.logger.error("send ok")


class WebSocketRPCFactory(websocket.WebSocketServerFactory):
    """Factory class for WebSocketRPCProtocol instances."""

    protocol = WebSocketRPCProtocol


def get_resource(secure=False, port=8080):
    """Build and return a WebSocketRPCResource instance."""
    protocol = "ws"
    if secure:
        protocol = "wss"
    connection_string = u"{}://0.0.0.0:{}".format(protocol, port)

    logger.Logger().error("connection_string: {}".format(connection_string))
    wsf = WebSocketRPCFactory(connection_string)
    wsf.protocol = WebSocketRPCProtocol
    # wsf.startFactory()
    return WebSocketRPCResource(wsf)


class EchoServerProtocol(websocket.WebSocketServerProtocol):
    """Simple test protocol for Websocket connections."""

    def onConnect(self, request):
        """Handle a new inbound connection."""
        print("WebSocket connection request: {}".format(request))

    def onMessage(self, payload, isBinary):
        """Handle a new inbound message."""

        if isinstance(payload, six.text_type):
            payload = payload.encode("utf-8")

        self.sendMessage(payload, isBinary)


class DSAAuthenticator(object):
    """Authenticate inbound requests using the signed payload."""

    def __init__(self, **kwargs):
        """Constructor."""
        self.verifier = dsa.Verifier(**kwargs)

        self.username_header = "assembly-elements-username"
        self.payload_header = "assembly-elements-payload"
        self.signature_header = "assembly-elements-signature"

    def authenticate_request(self, request):
        """Check the DSA signature on an inbound request."""
        username = request.requestHeaders.getRawHeaders(self.username_header, None)
        payload_bytes = request.requestHeaders.getRawHeaders(self.payload_header, None)
        signature = request.requestHeaders.getRawHeaders(self.signature_header, None)

        if None in (username, payload_bytes, signature):
            request.setResponseCode(HTTPStatus.NETWORK_AUTHENTICATION_REQUIRED)
            request.finish()
            return

        payload = json.loads(payload_bytes)

        signature = payload["signature"]
        timestamp = payload["timestamp"]
        uuid = payload["uuid"]

        message = json.dumps({"timestamp": timestamp, "uuid": uuid})

        self.verifier.verify_signature(self, username, message, signature)
