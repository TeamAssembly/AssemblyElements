#!/usr/bin/env python
"""Custom twisted.web.server.Site class."""

from assembly_elements.web import request
from assembly_elements.web import session

from twisted.web import server


class AssemblySite(server.Site):
    """
    Custom twisted.web.server.Site class.

    The class creates request instances using our custom factory, and is
    configured with a custom Session factory (although actual sessions are
    configured in the Request object class).
    """

    requestFactory = request.AssemblyRequest
    sessionFactory = session.AssemblySession

    @classmethod
    def set_session_adapter(cls, adapter):
        cls.sessionFactory.adapter_class = adapter
        cls.requestFactory.session_factory.adapter = adapter
