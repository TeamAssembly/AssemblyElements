#!/usr/bin/env python

import getpass
import logging
import time

from autobahn.twisted import websocket

from twisted import logger
from twisted.internet import defer
from twisted.internet import task

from assembly_elements.cryptography import dsa
from assembly_elements.user import User
from assembly_elements.util import jsondate
from assembly_elements.util import exceptions

from twisted.logger import Logger, STDLibLogObserver, globalLogPublisher

log = Logger()
logging.basicConfig(level=logging.DEBUG)

globalLogPublisher.addObserver(STDLibLogObserver())


class AssemblyWebSocketClientProtocol(websocket.WebSocketClientProtocol):

    notification_type = -2
    request_type = -1
    response_type = 0
    error_type = 1

    def __init__(self, *args, **kwargs):
        websocket.WebSocketClientProtocol.__init__(self, *args, **kwargs)

        self.logger = logger.Logger()
        self.logger.debug("AssemblyWebSocketClientProtocol Constructor")

        self.user = User(getpass.getuser())
        self.request_id = 0
        self.request_hash = {}
        self.signer = dsa.Signer()
        self.username = getpass.getuser()
        self.authenticated = defer.Deferred()

    def onConnect(self, response):
        self.logger.debug("onConnect", response=response)

    def log_error(self, f, msg="UNEXPECTED ERROR", trap=False):
        self.logger.error(msg, traceback=f.getBriefTraceback())
        if not trap:
            return f

    def authenticate(self):
        """
        Begin the authentication process:
            1: request a token
            2: add the token to a dict with username and timestamp
            3: JSON-encode the dict and sign the result
            4: forward the signature to the server for verification
        """
        self.logger.debug("begin authentication process")

        method = "request_token"
        args_list = [self.username]
        kwargs_dict = {}

        df = self.sendRequest(method, args_list, kwargs_dict)
        df.addCallback(self.sendSignedObject)
        return df

    def onClose(self, wasClean, code, reason):
        print("WebSocket connection closed: {0}".format(reason))

    def sendSignedObject(self, token_id):
        self.logger.debug("sendSignedObject", token_id=token_id)
        method = "authenticate"

        envelope = {"username": self.username, "token_id": token_id, "timestamp": time.time()}
        payload = jsondate.dumps(envelope)
        signature = self.signer.sign_message(self.username, payload).decode("utf-8")

        args_list = [signature, payload]
        kwargs_dict = {}
        df = self.sendRequest(method, args_list, kwargs_dict)
        df.addCallback(self.authenticationSucceeded)
        df.addErrback(self.authenticationFailed)
        return df

    def authenticationSucceeded(self, result):
        duration = time.time() - self.start_time
        self.logger.debug("authenticationSucceeded", result=result, duration=duration)
        if result == True:
            return self.authenticated.callback(True)
        return self.authenticated.errback(False)

    def authenticationFailed(self, result):
        self.logger.error("authenticationFailed")

    def onConnecting(self, transport_details):
        print("Connecting; transport details: {}".format(transport_details))
        return None  # ask for defaults

    def onOpen(self):
        self.logger.debug("onOpen")
        self.start_time = time.time()
        self.authenticate()

    def onMessage(self, payload, isBinary):
        """Respond to payload."""
        self.logger.debug("onMessage", payload=str(payload), isBinary=isBinary)
        message = jsondate.loads(payload)
        if message[0] == self.notification_type:
            self.processRequest(message)
        elif message[0] == self.request_type:
            self.processRequest(message, reply=True)
        elif message[0] == self.response_type:
            self.processResponse(message)
        else:
            self.processResponse(message)

    def processResponse(self, message):
        """Respond to an incoming RPC response."""
        self.logger.debug("processResponse")
        try:
            message_type, request_id, result = message
        except Exception as e:
            self.logger.error("no unpack", e=str(e))
            raise

        try:
            self.logger.error(
                "request_id", request_id=request_id, keys=self.request_hash.keys()
            )
            timeout, df = self.request_hash.pop(request_id)
        except Exception as e:
            self.logger.error("no pop", e=str(e))
            raise
        if not df:
            self.logger.error(
                "ERROR: No deferred was found for incoming response",
                request_id=request_id
            )
            return

        try:
            if message_type == self.response_type:
                df.callback(result)
            else:
                df.errback(result)
        except Exception as e:
            self.logger.error(
                "ERROR: Failed to callback with result for request", e=str(e)
            )

    def callRequestedMethod(self, name, *args, **kwargs):
        self.logger.debug("callRequestedMethod")
        if not name in self.factory.registry:
            rval = "method '{}' does not exist or has not been exported"
            raise exceptions.AssemblyWebSocketException(
                exceptions.AssemblyWebSocketException.no_such_method, rval
            )
        self.logger.error("keys", keys=self.factory.registry.keys())
        self.logger.error("name", name=name)
        try:
            if name == "authenticate":
                method = self.authenticate
            else:
                method = self.factory.registry[name]
            self.logger.error("method", method=str(method))
        except KeyError as e:
            self.logger.error("KeyError", e=str(e))
            self.logger.error(
                "no '{}' in '{}'".format(name, ",".join(self.factory.registry.keys()))
            )
            raise
        return defer.maybeDeferred(method, self.user, *args, **kwargs)

    def processRequest(self, message, reply=False):
        """Respond to an incoming RPC request."""
        self.logger.debug("processRequest:")
        try:
            _, request_id, method, args, kwargs = message
        except Exception as e:
            self.logger.error("exception", e=str(e))
            raise

        self.logger.debug(
            "request_id/method", request_id=request_id, method=str(method)
        )
        try:
            df = self.callRequestedMethod(method, *args, **kwargs)
            if reply:
                df.addCallback(self.sendResponse, request_id)
                df.addErrback(self.sendErrorResponse, request_id)
                df.addErrback(self.sendUncaughtErrorResponse, request_id)
                df.addErrback(self.log_error, "failed to process request")
            else:
                df.addErrback(self.log_error, "failed to process notification", trap=True)
        except Exception as e:
            self.logger.error("exception", e=str(e))
            raise

    def sendResponse(self, result, request_id):
        """Send a response back to the client."""
        self.logger.debug("sendResponse", request_id=request_id, result=str(result))
        response = (self.response_type, request_id, result)
        self.sendObject(response)

    def sendErrorResponse(self, failure, request_id):
        """Respond to an error."""
        self.logger.debug(
            "sendErrorResponse",
            request_id=request_id,
            result=str(failure.getBriefTraceback()),
        )
        failure.trap(exceptions.AssemblyWebSocketException)
        ex = failure.value
        response = (self.error_type, request_id, ex.errno, ex.message)
        self.sendObject(response)

    def sendUncaughtErrorResponse(self, failure, request_id):
        """
        Respond to an uncaught error.

        We discard the 'failure' object, because we *never* share any information
        about the internals of the application with the peer.
        """
        self.logger.debug(
            "sendUncaughtErrorResponse",
            request_id=request_id,
            failure=failure.getBriefTraceback(),
        )
        error_number = 1
        error_message = "unexpected error"
        response = (self.error_type, request_id, error_number, error_message)
        self.sendObject(response)

    def sendRequest(
        self, method, args_list, kwargs_dict, timeout=10
    ):
        """Send an RPC request to the peer."""
        return self.sendRequestOrNotification(self.request_type, method, args_list, kwargs_dict, timeout)

    def sendNotification(
        self, method, args_list, kwargs_dict, timeout=10
    ):
        """Send an RPC notification to the peer."""
        return self.sendRequestOrNotification(self.notification_type, method, args_list, kwargs_dict, timeout)

    def sendRequestOrNotification(
        self, message_type, method, args_list, kwargs_dict, timeout=10
    ):
        """Send an RPC request to the peer."""
        self.logger.debug(
            "sendRequest", method=method, args_list=args_list, kwargs_dict=kwargs_dict
        )

        request_id = self.request_id
        self.request_id += 1

        df = None
        if message_type == self.request_type:
            df = defer.Deferred()
            timeout = time.time() + timeout
            self.request_hash[request_id] = (timeout, df)

        request = [message_type, request_id, method, args_list, kwargs_dict]
        self.sendMessage(jsondate.dumps(request).encode("utf-8"), isBinary=False)
        return df

    def sendObject(self, obj):
        """Encode an object and send it to the peer."""
        return self.sendMessage(jsondate.dumps(obj).encode("utf-8"), isBinary=False)

    def timeoutOldRequests(self):
        """
        Every ten seconds our ProtocolFactory calls this method to sweep expired requests.
        """
        now = time.time()
        for key in self.request_hash.keys():
            timeout, df = self.request_hash[key]
            if timeout > now:
                self.request_hash.pop(key)
            try:
                df.errback("timeout")
            except Exception as e:
                self.logger.error("ERROR: Failed to errback on timeout: {}".format(e))


class AssemblyWebSocketClientFactory(websocket.WebSocketClientFactory):

    logger = logger.Logger()
    protocol = AssemblyWebSocketClientProtocol

    def __init__(self, *args, **kwargs):
        websocket.WebSocketClientFactory.__init__(self, *args, **kwargs)

        self.setProtocolOptions(autoPingInterval=10, autoPingTimeout=10)
        self._protocols = set()
        self.timeout_loop = None
        self.beginLooping()
        self.registry = {
                "alert_user": self.alert_user
            }

    def alert_user(self, user, *args, **kwargs):
        message = args[0]
        print("\n\n{}\n\n".format(message))

    def connectionLost(self, connection):
        self.logger.debug("AssemblyWebSocketClientFactory.connectionLost")
        try:
            self._protocols.remove(connection)
        except Exception:
            self.logger.error(
                "ERROR: Tried to remove connection from factory it doesn't belong to."
            )

    def buildProtocol(self, addr):
        self.logger.debug("AssemblyWebSocketClientFactory.buildProtocol")
        protocol = websocket.WebSocketClientFactory.buildProtocol(self, addr)
        self._protocols.add(protocol)
        return protocol

    def beginLooping(self):
        if self.timeout_loop is not None:
            return

        self.timeout_loop = task.LoopingCall(self.timeoutOldRequests)
        self.timeout_loop.start(10, False)

    def timeoutOldRequests(self):
        for protocol in self._protocols:
            protocol.timeoutOldRequests()

    def registerMethod(self, name, method):
        """Register a method name and a method as available for remote execution."""
        self.logger.debug("registerMethod: {} :: {}".format(name, method))
        if name in self.registry:
            raise exceptions.AssemblyException(
                "method name '{}' is already represented in "
                "AssemblyWebSocketClientFactory.registry by '{}'".format(
                    name, self.registry[name]
                )
            )
        self.registry[name] = method

    def testResponseMethod(self, *args, **kwargs):
        """Return a success result to the client for testing."""
        self.logger.debug("testResponseMethod")
        return {"args": args, "kwargs": kwargs, "extras": [1, 2, 3]}

    def testErrorResponseMethod(self, *args, **kwargs):
        """Return an error result to the client for testing."""
        self.logger.debug("testErrorResponseMethod")
        rval = "an error occurred and your request could not be completed"
        raise exceptions.AssemblyWebSocketException(
            exceptions.AssemblyWebSocketException.unexpected_error, rval
        )
