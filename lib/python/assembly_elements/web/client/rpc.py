#!/usr/bin/env python

from __future__ import unicode_literals

import getpass
import requests
import traceback
import umsgpack

from twisted import logger

from assembly_elements.util import jsondate
from assembly_elements.cryptography.dsa import signer


class RPCClient(object):
    logger = logger.Logger()
    counter = 0

    def __init__(
        self,
        hostname,
        protocol="https",
        port=443,
        path=None,
        dsa_key_base_directory=None,
        dsa_key_leaf_directory=None,
    ):
        self.dsa_signer = signer.Signer(
            base_directory=dsa_key_base_directory,
            leaf_directory=dsa_key_leaf_directory,
        )

        self.endpoint = "{}://{}".format(protocol, hostname)
        if (protocol == "https" and port != 443) or (protocol == "http" and port != 80):
            self.endpoint = "{}:{}".format(self.endpoint, port)

        if not path:
            path = "/"
        elif not path.startswith("/"):
            path = "/" + path
        self.endpoint = "{}{}".format(self.endpoint, path)
        self.username = getpass.getuser().lower()
        self.session = requests.session()
        self.session_cookie = None
        self.session_cookie_name = None

    def next_id(self):
        RPCClient.counter += 1
        return RPCClient.counter

    def get_token(self):
        try:
            url = "{}?token={}".format(self.endpoint, self.username)
            response = self.session.get(url)

            for cookie in response.cookies:
                if cookie.name.endswith("_SESSION"):
                    self.session_cookie_name = cookie.name
                    self.session_cookie = cookie
                    break

        except Exception as e:
            self.logger.error("failed to call jsonrpc server", e=str(e))
            raise

        response_dict = self.decode_payload(response.content)
        return response_dict["data"]

    def submit(self, body, retry=False):
        must_authenticate = (
            self.session_cookie is None or
            self.session_cookie.is_expired()
        )

        body["id"] = self.next_id()
        body["jsonrpc"] = "2.0"

        payload = self.encode_payload(body)

        headers = {
            "Content-type": self.content_type,
            "Accept": "text/plain",
            "x-assembly-username": self.username
        }

        if must_authenticate:
            """
            We can identify our user to the server by signing the payload with a
            private key that only the signer has permission to read.
            """
            token = self.get_token().encode("utf-8")
            signature = self.dsa_signer.sign_message(self.username, token + payload)
            headers["x-assembly-signature"] = signature
            headers["x-assembly-token"] = token

        try:
            response = self.session.post(self.endpoint, data=payload, headers=headers)
            for cookie in response.cookies:
                if cookie.name.endswith("_SESSION"):
                    self.session_cookie_name = cookie.name
                    self.session_cookie = cookie
                    break

        except Exception as e:
            self.logger.error("post request failed", e=str(e), traceback=traceback.format_exc())
            raise

        try:
            result = self.decode_payload(response.content)
            if result.get("error") == "must reauthenticate":
                self.session_cookie = None
                if not retry:
                    return self.submit(body, retry=True)
            return result
        except Exception as e:
            self.logger.error("failed to return response.content: {}".format(e))
            raise


class MsgpackRPCClient(RPCClient):
    content_type = "application/msgpack"

    def encode_payload(self, *args, **kwargs):
        return umsgpack.packb(*args, **kwargs)

    def decode_payload(self, *args, **kwargs):
        return umsgpack.unpackb(*args, **kwargs)


class JsonRPCClient(RPCClient):
    content_type = "application/json"

    def encode_payload(self, *args, **kwargs):
        return jsondate.dumps(*args, **kwargs).encode("utf-8")

    def decode_payload(self, *args, **kwargs):
        return jsondate.loads(*args, **kwargs)

