#!/usr/bin/env python
"""A custom HTTP request object that manages its own session data."""

import binascii
import os
import traceback
from collections import namedtuple

from assembly_elements.web import session
from assembly_elements.web.session import adapter


from twisted import logger
from twisted.internet import defer


SessionParameters = namedtuple("SessionParameters", ("cookie_name", "is_secure", "domain", "sameSite"))


class SessionFactory(object):
    """
    The standard Request object does not return a deferred from getSession.

    This implemetation returns a deferred from getSession. It's not how
    sessions are designed to work in twisted web, but it's simple and direct.
    """

    logger = logger.Logger()
    adapter_class = adapter.AssemblySessionAdapter

    def __init__(self):
        self.adapter = self.__class__.adapter_class
        self.session_class = session.AssemblySession
        self.session_class.adapter = self.adapter

        self.secure_cookie = b"ASSEMBLY_SECURE_SESSION"
        self.insecure_cookie = b"ASSEMBLY_SESSION"

        self.sameSite = "Strict"
        self.session_cookie_domain = None
        self.using_tls_proxy = False

        self.secure_session_parameters = SessionParameters(
            self.secure_cookie,
            True,
            self.session_cookie_domain,
            None
        )
        self.insecure_session_parameters = SessionParameters(
            self.insecure_cookie,
            self.using_tls_proxy,
            self.session_cookie_domain,
            None
        )

    def log_error(self, f, label="logging_errback"):
        """Log an error without trapping it."""
        self.logger.error(label, msg=f.getBriefTraceback())
        return f

    def touch_session(self, request):
        """Check that the session is valid, touch it, and return it."""
        session = request.assembly_session
        self.logger.debug("touch_session: {}".format(session))

        if not session or session.expired:
            return self.make_session(request)

        session.touch()
        return request

    def update_cookie(self, session, request):
        """Update the headers to set the session cookie for this request."""
        self.logger.debug("update_cookie", session_data=session.attributes)
        if not session:
            msg = "after creating a new session, the Session value was still False"
            self.logger.error(msg)
            raise Exception(msg)

        try:
            request.addCookie(
                request.assembly_session_parameters.cookie_name,
                session.session_id,
                path=b"/",
                secure=request.assembly_session_parameters.is_secure,
                sameSite=request.assembly_session_parameters.sameSite,
                domain=request.assembly_session_parameters.domain
            )
        except Exception:
            self.logger.error("failed to set session cookie", t=traceback.format_exc())

        return session

    def catch_invalid_session(self, failure, request):
        """Catch 'session_id doesn't exist', and create a fresh session."""
        failure.trap(session.AssemblyInvalidSessionIdException)
        self.logger.debug("replace_invalid_session")
        return self.make_session(request)

    def catch_expired_session(self, failure, request):
        """Catch 'session has expired', and create a fresh session."""
        failure.trap(session.AssemblySessionExpiredException)
        self.logger.debug("replace_expired_session")
        return self.make_session(request)

    def get_session(self, request, session_id, session_parameters):
        """Return the session object for a given session_id."""
        self.logger.debug("get_session: {}".format(session_id))
        df = self.adapter.get(session_id)
        df.addCallback(self.session_class, self, session_parameters)
        return df

    def create_session_id(self):
        return binascii.hexlify(os.urandom(32))

    def make_session(self, request, username=None, attributes=None):
        """Create and return a new session instance."""
        self.logger.debug("make_session: {}".format(request))

        parameters = self.get_session_parameters(request)
        session_id = self.create_session_id()

        if attributes is None:
            attributes = {}

        df = self.adapter.create(
            session_id,
            username,
            attributes,
            parameters.cookie_name,
            parameters.is_secure,
        )
        df.addCallback(self.session_class, self, parameters)
        df.addCallback(self.update_cookie, request)
        df.addCallback(self.set_session_attribute, request)
        df.addCallback(lambda x: request)
        df.addErrback(self.log_error, "failed to make session")
        return df

    def set_session_attribute(self, session, request):
        """
        Attach the session object to the correct attribute name.

        Depending on whether this request is serving a TLS or plain TCP request, the
        session object will be stored in one or other attribute:
            self._insecureSession
            self._secureSession

        It is important not to mix these up.
        """
        self.logger.debug(
            "set_session_attribute: session: {} request: {}".format(session, request)
        )
        try:
            request.set_assembly_session(session)
        except Exception:
            self.logger.error(
                "failed to set assembly session: {}".format(traceback.format_exc())
            )
            raise

        return request

    def get_session_parameters(self, request):
        """
        Return the cookie name and is_secure session attribute for this request.

        TLS:
            secure_cookie = b"ASSEMBLY_SECURE_SESSION"
            is_secure = True
        TCP:
            insecure_cookie = b"ASSEMBLY_SESSION"
            is_secure = False
        """
        self.logger.debug("get_session_parameters: {}".format(request))
        if request.assembly_session_parameters:
            return request.assembly_session_parameters

        is_secure = request.isSecure()

        if is_secure:
            request.set_assembly_session_parameters(self.secure_session_parameters)
        else:
            request.set_assembly_session_parameters(self.insecure_session_parameters)

        return request.assembly_session_parameters

    def getSharedSession(self, request):
        """
        Check if there is a session cookie, and if not, create it.

        The cookie will be secure for HTTPS requests and not secure for HTTP requests.
        """
        self.logger.debug("getSharedSession")
        session_parameters = self.get_session_parameters(request)
        self.logger.debug("params: {}".format(request.assembly_session_parameters))

        session = request.assembly_session

        if session:
            session.touch()
            return defer.succeed(request)

        session_id = request.getCookie(session_parameters.cookie_name)

        if not session_id:
            # We have never set a cookie for this user
            self.logger.debug("user had no session cookie")
            return self.make_session(request)

        self.logger.debug("user had cookie: {}".format(session_id))
        session_df = self.get_session(request, session_id, session_parameters)
        session_df.addCallback(self.set_session_attribute, request)
        session_df.addErrback(self.catch_invalid_session, request)
        session_df.addErrback(self.catch_expired_session, request)
        session_df.addCallback(self.touch_session)
        session_df.addErrback(self.log_error, "unspecified session error")
        return session_df
