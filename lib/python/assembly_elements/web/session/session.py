#!/usr/bin/env python
"""Assembly Elements Web Session Object."""

from twisted import logger
from twisted.web import server

from . import exceptions as session_ex


class AssemblySession(server.Session):
    """Assembly Elements Web Session Object."""

    logger = logger.Logger()
    adapter = None

    def __init__(self, result, factory, parameters):
        """Constructor."""
        self.logger.debug("AssemblySession")
        try:
            self.session_id = result["session_id"]
            self.__username = result["username"]
            self.attributes = result["attributes"]
            self.modified = result["modified"]
            self.expired = False
        except Exception as e:
            self.logger.error("failed to unpack session: {}".format(e))

        self.factory = factory
        self.adapter = factory.adapter
        """
        self.parameters is a named tuple:
            self.parameters.cookie_name
            self.parameters.is_secure
        """
        self.parameters = parameters

    @property
    def username(self):
        return self.__username

    @username.setter
    def username(self, value):
        raise Exception("AssemblySession.username can not be set")

    @username.deleter
    def username(self):
        raise Exception("AssemblySession.username can not be deleted")

    def expire(self, session_id=None):
        """Force a session to expire. The data is deleted."""
        self.logger.debug(
            "AssemblySession.expire: {} v {}".format(session_id, self.session_id)
        )

        if session_id is None:
            session_id = self.session_id

        session_df = self.adapter.delete(session_id)
        session_df.addErrback(self.log_error)
        return session_df

    def log_error(self, f):
        """Log an error without trapping it."""
        self.logger.error("AssemblySession error: {}".format(f.getBriefTraceback()))
        return f

    def touch(self):
        """Refresh the modify-time on the session."""
        self.adapter.touch(self.session_id)

    def update(self, attributes=None, new_username=None):
        """Update the data for the session."""
        self.logger.debug("update", attributes=attributes)

        if new_username and self.username and new_username != self.username:
            msg = (
                "SECURITY VIOLATION: attempt to escalate privileges from "
                "user {} to {}".format(self.username, new_username)
            )
            self.logger.error(msg)
            raise session_ex.AssemblyInvalidUserException(msg)

        if not new_username:
            new_username = self.username

        if attributes is None:
            attributes = self.attributes

        session_df = self.adapter.update(self.session_id, new_username, attributes)
        session_df.addErrback(self.log_error)
        return session_df

    def authenticate(self, username, attributes):
        """Update the data for the session, and assign a new session_id."""
        new_session_id = self.factory.create_session_id()
        session_df = self.adapter.authenticate(
            self.session_id, username, new_session_id, attributes
        )
        session_df.addCallback(self._authenticate_ok, username, new_session_id, attributes)
        session_df.addErrback(self.log_error)
        return session_df

    def _authenticate_ok(self, result, username, new_session_id, attributes):
        """On successful authentication, adopt the new session_id and attributes."""
        self.session_id = new_session_id
        self.attributes = attributes
        self.__username = username
        return self
