#!/usr/bin/env python
"""Custom sessions for Assembly."""

from assembly_elements.util import exceptions


class AssemblySessionException(exceptions.AssemblyException):
    """Base class for Assembly Session Errors."""


class AssemblySessionExpiredException(AssemblySessionException):
    """Raised when we try to get data from an expired session."""


class AssemblyInvalidSessionIdException(AssemblySessionException):
    """Raised when a user tries to re-use a session_id."""


class AssemblyInvalidUserException(AssemblySessionException):
    """A user attempted to raise privileges."""
