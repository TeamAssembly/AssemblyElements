#!/usr/bin/env python
"""Basic session data persistence adapter."""

import datetime
import time

from assembly_elements.web.session import exceptions as session_ex

from twisted import logger
from twisted.internet import defer


class AssemblySessionAdapter(object):
    """
    Basic session data persistence adapter.

    This reference implementation stores sessions in local memory, but all requests
    for session information are asynchronous so that this class can easily be swapped
    for one that stores information in some kind of database.
    """

    sessions = {}
    sessionTimeout = datetime.timedelta(days=1)
    logger = logger.Logger()

    @classmethod
    def delete(cls, session_id):
        """Delete a session if it exists."""
        cls.sessions.pop(session_id, None)
        return defer.succeed(0)

    @classmethod
    def authenticate(cls, old_session_id, username, new_session_id, attributes):
        """
        When a user authenticates, change the session ID and store the data.

        Forcing the session_id to change helps to mitigate session fixation attacks.
        https://www.owasp.org/index.php/Session_fixation
        """
        try:
            cls.check_expired(old_session_id)
        except Exception as e:
            return defer.fail(e)

        old_session = cls.sessions.pop(old_session_id, None)
        if old_session is None:
            """
            Are you trying to implement authentication that doesn't require a session to exist?
            That will require some re-work here.
            """
            raise session_ex.AssemblySessionException("trying to authenticate a non-existant session")

        cookie_name = old_session["cookie_name"]
        is_secure = old_session["is_secure"]
        return cls.create(new_session_id, username, attributes, cookie_name, is_secure)

    @classmethod
    def create(cls, session_id, username, attributes, cookie_name, is_secure):
        """Store a new session."""
        if session_id in cls.sessions:
            raise session_ex.AssemblyInvalidSessionIdException()

        cls.sessions[session_id] = {
            "session_id": session_id,
            "username": username,
            "attributes": attributes,
            "modified": time.time(),
            "cookie_name": cookie_name,
            "is_secure": is_secure,
        }
        return defer.succeed(cls.sessions[session_id])

    @classmethod
    def touch(cls, session_id):
        """Update the 'modified' time for a session and store it."""
        cls.sessions[session_id]["modified"] = time.time()
        return defer.succeed(cls.sessions[session_id])

    @classmethod
    def get(cls, session_id):
        """Return the session data for a given session_id."""
        try:
            result = cls.check_expired(session_id)
        except Exception as e:
            return defer.fail(e)
        return defer.succeed(result)

    @classmethod
    def update(cls, session_id, username, attributes):
        """Update the data for a given session and refresh the 'modify' time."""
        try:
            cls.check_expired(session_id)
        except Exception as e:
            return defer.fail(e)
        cls.sessions[session_id].update({
            "username": username,
            "attributes": attributes,
            "modified": time.time(),
        })
        return defer.succeed(cls.sessions[session_id])

    @classmethod
    def check_expired(cls, session_id):
        """Return the data for a session_id. Raise an exception if it's not ok.

        A session may have expired, or just be somehow invalid. In the case of any
        problem we raise an exception.
        """
        data = cls.sessions.get(session_id)
        if not data or "modified" not in data:
            cls.logger.debug("unrecognised session: {}".format(session_id))
            raise session_ex.AssemblyInvalidSessionIdException()

        modified = data["modified"]

        now = time.time()
        expire_time = modified + cls.sessionTimeout.total_seconds()
        expired = expire_time < now

        if expired:
            cls.sessions.pop(session_id)
            raise session_ex.AssemblySessionExpiredException()

        data["modified"] = now
        return data.copy()
