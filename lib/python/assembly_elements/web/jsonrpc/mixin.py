#!/usr/bin/env python

from contextlib import contextmanager

import re
import umsgpack

from assembly_elements.util import jsondate

from twisted.internet import defer


# JsonRPC Standard Exceptions -----------------------------------------------------


class JsonRpcException(Exception):

    json_parse_error = (-32700, "Parse error")
    invalid_request = (-32600, "Invalid Request")
    invalid_params = (-32602, "Invalid params")
    internal_error = (-32603, "Internal error")
    method_not_found = (-32601, "Method not found")

    def __str__(self):
        return "request_id: {}  errno: {}  message: {}".format(self.request_id, self.errno, self.message)

class JsonRpcParseError(JsonRpcException):
    def __init__(self, request_id):
        self.request_id = request_id
        self.errno, self.message = self.json_parse_error

class JsonRpcInvalidRequest(JsonRpcException):
    def __init__(self, request_id):
        self.request_id = request_id
        self.errno, self.message = self.invalid_request

class JsonRpcInvalidParams(JsonRpcException):
    def __init__(self, request_id):
        self.request_id = request_id
        self.errno, self.message = self.invalid_params

class JsonRpcInternalError(JsonRpcException):
    def __init__(self, request_id):
        self.request_id = request_id
        self.errno, self.message = self.internal_error

class JsonRpcMethodNotFound(JsonRpcException):
    def __init__(self, request_id):
        self.request_id = request_id
        self.errno, self.message = self.method_not_found


# Batch Requests to send in one hit -----------------------------------------------


class RequestBatcher(object):
    def __init__(self, parent):
        self.parent = parent
        self.requests = []

    def addRequest(self, method, params=None):
        payload, df = self.parent.build_request(method, params=params)
        self.requests.append(payload)

    def addNotification(self, method, params=None):
        notification = self.parent.build_notification(method, params=params)
        self.requests.append(notification)

    def sendBatch(self):
        self.parent.sendObject(self.requests)


# JsonRPC Protocol Implementation -------------------------------------------------


class RpcProtocolMixin(object):

    request_id_regex = re.compile("\"id\"\s*:\s*(\d+)")

    def __init__(self, *args, **kwargs):
        super(RpcProtocolMixin, self).__init__(*args, **kwargs)
        self.next_request_id = 0

    @contextmanager
    def batchRequest(self):
        thing = RequestBatcher(self)
        try:
            yield RequestBatcher(self)
        finally:
            thing.close()


    def handleParseError(self, e):
        if not isinstance(e, JsonRpcException):
            # TODO: Log the real exception
            e  = JsonRpcParseError(None)

        envelope = {
            "jsonrpc": "2.0",
            "error": {
                "code": e.code,
                "message": e.message
            },
            "id": e.request_id
        }
        return self.sendObject(envelope)

    def onOpen(self):
        print("ready")

    def onMessage(self, message, isBinary):
        """Unpack and process message."""
        try:
            payload = self.unpack(message)
        except Exception as e:
            return self.handleParseError(e)

        if isinstance(payload, (list, tuple)):
            if "request" in payload[0]:
                return self.onBatchRequest(payload)
            else:
                return self.onBatchResponse(payload)

        if "method" in payload:
            df = self.onRequest(**payload)
            df.addCallback(self.sendObject)
            return df

        if "result" in payload:
            return self.onResult(payload)

        if "error" in payload:
            return self.onError(payload)

        """
        If no methods matched, then we have an invalid request.
        """
        request_id = payload.get("id")
        return self.handleParseError(JsonRpcInvalidRequest(request_id))

    def next_id(self):
        """Return the next available request_id."""
        next_request_id = self.next_request_id
        self.next_request_id += 1
        return next_request_id

    def request(self, method, params=None):
        """
        Send a request to the peer.

        A request requires a response and must have an "id"
        """
        payload, df = self.build_request(method, params=params)
        self.sendObject(payload)
        return df

    def build_request(self, method, params=None):
        """
        Build a request to send to the peer.

        A request requires a response and must have an "id"
        """
        request_id = self.next_id()
        payload = {
            "jsonrpc": "2.0",
            "method": method,
            "id": request_id
        }
        if params:
            payload["params"] = params

        df = defer.Deferred()
        self.deferreds[request_id] = df
        return payload, df

    def notify(self, method, params=None):
        """
        Send a notification to the peer.

        A notification does not require a response and must not have an "id"
        """
        notification = self.build_notification(method, params=params)
        self.sendObject(notification)

    def build_notification(self, method, params=None):
        """
        Send a notification to the peer.

        A notification does not require a response and must not have an "id"
        """
        payload = {
            "jsonrpc": "2.0",
            "method": method
        }

        if params:
            payload["params"] = params

        return payload

    def onResult(self, payload):
        result = payload["result"]
        request_id = payload["id"]
        df = self.deferreds.get(request_id)
        df.callback(result)

    def onError(self, payload):
        result = payload["error"]
        request_id = payload["id"]
        df = self.deferreds.get(request_id)
        df.errback(result)

    def onRequest(self, jsonrpc=None, method=None, params=None, id=None):
        if isinstance(params, (list, tuple)):
            result = method(*params)
        else:
            result = method(**params)

        if id is None:
            # A request with no id is a notification, and doesn't need a response.
            return None

        df = defer.maybeDeferred(result)
        df.addCallback(self.packageResult, id)
        df.addErrback(self.packageFailureObject, id)
        return df

    def onBatchResult(self, batch):

        for result in batch:
            request_id = result.get("id")
            if not request_id:
                continue
            df = self.deferreds[request_id]

            if "error" in result:
                df.errback(result.get("error"))
            elif "result" in result:
                df.callback(result.get("result"))

    def onBatchRequest(self, batch):
        results = []
        for request in batch:
            result = self.onRequest(**request)
            if result is None:
                continue
            results.append(result)

        df = defer.gatherResults(results)
        df.addCallback(self.sendObject)
        return df

    def packageResult(self, result, request_id):
        envelope = {
            "jsonrpc": "2.0",
            "result": result,
            "id": request_id
        }
        return envelope

    def packageFailureObject(self, failure, request_id):
        return self.packageExceptionObject(failure.value, request_id)

    def packageExceptionObject(self, exception, request_id):
        try:
            code, message, request_id = (
                exception.code, exception.message, exception.request_id
            )
        except Exception:
            code, message = JsonRpcException.internal_error

        if request_id is None:
            return

        envelope = {
            "jsonrpc": "2.0",
            "error": {
                "code": code,
                "message": message
            },
            "id": request_id
        }
        return envelope


# Msgpack and Json Implementations ------------------------------------------------


class MsgpackRpcProtocolMixin(RpcProtocolMixin):

    def unpack(self, msgpackdata):
        try:
            return umsgpack.unpackb(msgpackdata)
        except:
            raise JsonRpcParseError(None)

    def sendObject(self, data):
        self.sendMessage(umsgpack.packb(data))


class JsonRpcProtocolMixin(RpcProtocolMixin):

    def unpack(self, jsondata):
        try:
            return jsondate.loads(jsondata)
        except Exception:
            pass

        """
        Because json unpack failed, we don't know the request ID to add to our
        error message. This regex tries to salvage an ID from the broken javascript,
        and succeeds only if it finds exactly one match.
        """
        matches = self.request_id_regex.findall(jsondata)
        if len(matches) == 1:
            request_id = int(matches[0])
            raise JsonRpcParseError(request_id)

        raise JsonRpcParseError(None)

    def sendObject(self, data):
        self.sendMessage(jsondate.dumps(data).encode("utf-8"))


