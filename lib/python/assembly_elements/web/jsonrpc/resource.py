#!/usr/bin/env python

from assembly_elements.web.jsonrpc import mixin

from autobahn.twisted import websocket
from autobahn.twisted import resource



class JsonRpcServerProtocol(mixin.JsonRpcProtocolMixin, websocket.WebSocketClientProtocol):
    pass


def JsonRpcResource():
    websocket_factory = websocket.WebSocketServerFactory()
    websocket_factory.protocol = JsonRpcServerProtocol
    websocket_factory.startFactory()  # when wrapped as a Twisted Web resource, start the underlying factory manually
    websocket_resource = resource.WebSocketResource(websocket_factory)
    return websocket_resource

