#!/usr/bin/env python

from collections import namedtuple
import time
import uuid

from twisted.internet import defer
from twisted import logger

from assembly_elements.util import jsondate
from assembly_elements.util import exceptions
from assembly_elements.cryptography import dsa


Token = namedtuple("Token", ("id", "expires", "username"))

class BaseAuth(object):
    tokens = {}
    token_expires = 5  # Base auth tokens are valid for 5 seconds
    logger = logger.Logger()

    @classmethod
    def log_error(cls, f, message="unexpected error", **kwargs):
        cls.logger.failure(message, failure=f, **kwargs)
        return f

    @classmethod
    def request_token(cls, username, *args, **kwargs):
        """Return a unique token for 'username' (which we cache)."""
        cls.logger.debug("BaseAuth.request_token", _args=args, _kwargs=kwargs)

        while True:
            unique_token_id = uuid.uuid4().hex
            if unique_token_id not in cls.tokens:
                break

        expires = cls.token_expires + time.time()
        token = Token(unique_token_id, expires, username)
        cls.logger.debug("token", token=token)
        cls.tokens[token.id] = token
        return defer.succeed(token.id)

    @classmethod
    def expire_keys(cls):
        """Delete expired tokens from the cache."""
        cls.logger.debug("BaseAuth.expire_keys")
        now = time.time()
        for token in cls.tokens.values():
            if token.expires <= now:
                del cls.tokens[token.id]

    @classmethod
    def get_token_for_id(cls, token_id):
        token = cls.tokens.pop(token_id, None)
        return defer.succeed(token)

    @classmethod
    def check_token(cls, token_id, username, client_timestamp=None):
        token_df = cls.get_token_for_id(token_id)
        token_df.addCallback(cls._check_token, username, client_timestamp)
        token_df.addErrback(cls.log_error, "failed to get token for token_id", token_id=token_id)
        return token_df

    @classmethod
    def _check_token(cls, token, username, client_timestamp=None):

        if not token:
            cls.logger.error("no token '{}' in '{}'".format(token_id, cls.tokens.keys()))
            raise exceptions.AssemblyWebSocketAuthenticationException(
                exceptions.AssemblyWebSocketException.unrecognised_token,
                "auth failed"
            )

        now = time.time()
        if token.expires < now:
            raise exceptions.AssemblyWebSocketAuthenticationException(
                exceptions.AssemblyWebSocketException.expired_token,
                "auth failed"
            )

        if client_timestamp and client_timestamp + cls.token_expires < now:
            raise exceptions.AssemblyWebSocketAuthenticationException(
                exceptions.AssemblyWebSocketException.bad_timestamp,
                "auth failed"
            )

        if token.username != username:
            cls.logger.error("username_mismatch", token_username=token.username, username=username, token_username_type=str(type(token.username)), username_type=str(type(username)))
            raise exceptions.AssemblyWebSocketAuthenticationException(
                exceptions.AssemblyWebSocketException.username_mismatch,
                "auth failed"
            )

        return username

    @classmethod
    def authenticate(cls, *args, **kwargs):
        """Check the user's supplied credentials."""
        raise NotImplementedError


class AssemblyLdapAuth(BaseAuth):
    tokens = {}
    token_expires = 90  # Auth tokens are valid for 90 seconds
    logger = logger.Logger()

    @classmethod
    def authenticate(cls, username, password, token):
        """Check the user's supplied credentials."""
        cls.logger.debug("LdapAuth.authenticate", username=username)
        raise NotImplementedError


class AssemblyEcdsaAuth(BaseAuth):
    tokens = {}
    token_expires = 5  # Auth tokens are valid for 5 seconds
    verifier = dsa.Verifier()
    logger = logger.Logger()

    @classmethod
    def authenticate(cls, user, signature, payload):
        """Check the user's supplied credentials."""
        cls.logger.debug("AssemblyEcdsaAuth.authenticate", payload=payload)
        envelope = jsondate.loads(payload)
        username = envelope.get("username")
        token_id = envelope.get("token_id")
        timestamp = envelope.get("timestamp")

        cls.check_token(username, token_id, client_timestamp=timestamp)

        try:
            cls.verifier.verify_signature(
                username,
                payload.encode("utf-8"),
                signature.encode("utf-8")
            )
        except Exception as e:
            cls.logger.error(
                "Failed auth for user '{}' bad signature: {}".format(username, e),
                payload=payload,
                signature=signature,
                ptype=str(type(payload)),
                stype=str(type(signature))
            )
            raise exceptions.AssemblyWebSocketAuthenticationException(
                exceptions.AssemblyWebSocketException.bad_signature,
                "auth failed"
            )

        return username
