#!/usr/bin/env python

from autobahn.twisted import resource as ab_resource

from twisted import logger
from twisted.web import resource
from twisted.web import server
from twisted.protocols import policies
from zope.interface import implementer

#SECURITY : WHAT DO WE NEED TO DO WITH WEB SESSION COOKIE STUFF THAT MIGHY HAVE HAPPENED IN THE SESSION HANDLER

@implementer(resource.IResource)
class AssemblyWebSocketResource(ab_resource.WebSocketResource):

    logger = logger.Logger()

    def log_error(self, f, msg="UNEXPECTED ERROR"):
        self.logger.error(msg, traceback=f.getBriefTraceback())
        return f

    def render(self, request):
        """
        Render the resource. This will takeover the transport underlying
        the request, create a :class:`autobahn.twisted.websocket.WebSocketServerProtocol`
        and let that do any subsequent communication.
        """
        self.logger.debug(str(request.__class__))
        # for reasons unknown, the transport is already None when the
        # request is over HTTP2. request.channel.getPeer() is valid at
        # this point however
        if request.channel.transport is None:
            # render an "error, you're doing HTTPS over WSS" webpage
            from autobahn.websocket import protocol
            request.setResponseCode(426, b"Upgrade required")
            # RFC says MUST set upgrade along with 426 code:
            # https://tools.ietf.org/html/rfc7231#section-6.5.15
            request.setHeader(b"Upgrade", b"WebSocket")
            html = protocol._SERVER_STATUS_TEMPLATE % ("", protocol.__version__)
            return html.encode('utf8')

        # Create Autobahn WebSocket protocol.
        #
        protocol = self._factory.buildProtocol(request.transport.getPeer())
        if not protocol:
            # If protocol creation fails, we signal "internal server error"
            request.setResponseCode(500)
            return b""

        session_df = request.getSharedSession()
        session_df.addCallback(self._render, protocol)
        session_df.addErrback(self.log_error)
        return server.NOT_DONE_YET

    def _render(self, request, protocol):
        # Extract and save the AssemblySession object
        #
        protocol.setSessionObject(request.assembly_session)

        # Take over the transport from Twisted Web
        #
        transport, request.channel.transport = request.channel.transport, None

        # Connect the transport to our protocol. Once #3204 is fixed, there
        # may be a cleaner way of doing this.
        # http://twistedmatrix.com/trac/ticket/3204
        #
        if isinstance(transport, policies.ProtocolWrapper):
            # i.e. TLS is a wrapping protocol
            transport.wrappedProtocol = protocol
        else:
            transport.protocol = protocol
        protocol.makeConnection(transport)

        # On Twisted 16+, the transport is paused whilst the existing
        # request is served; there won't be any requests after us so
        # we can just resume this ourselves.
        # 17.1 version
        if hasattr(transport, "_networkProducer"):
            transport._networkProducer.resumeProducing()
        # 16.x version
        elif hasattr(transport, "resumeProducing"):
            transport.resumeProducing()

        # We recreate the request and forward the raw data. This is somewhat
        # silly (since Twisted Web already did the HTTP request parsing
        # which we will do a 2nd time), but it's totally non-invasive to our
        # code. Maybe improve this.
        #
        content = request.content.read()

        newline = b"\x0d\x0a"

        data = [
            request.method,
            b" ",
            request.uri,
            b" HTTP/1.1",
            newline
        ]

        for key, values in request.requestHeaders.getAllRawHeaders():
            data.extend(
                [key,
                 b": ",
                 b",".join(values),
                 newline]
            )

        data.extend([newline, content])
        data = b"".join(data)

        protocol.dataReceived(data)

        return server.NOT_DONE_YET
