#!/usr/bin/env python
"""Basic web page that uses the custom session code."""

import os

from twisted.web import static

ASSEMBLY_ELEMENTS_BASEDIR = os.environ.get("ASSEMBLY_ELEMENTS_BASEDIR")


def get_favicon(filepath=None):
    """Return the bytes of the favicon file for efficient despatch."""
    if not filepath:
        filepath = os.path.join(ASSEMBLY_ELEMENTS_BASEDIR, "static/img/favicon/favicon.ico")
    if not os.path.exists(filepath):
        raise Exception("Security Warning: Attempt to serve unknown static directory")

    favicon_data = b""
    with open(filepath, "rb") as fh:
        favicon_data = fh.read()
    return static.Data(favicon_data, "image/x-icon")
