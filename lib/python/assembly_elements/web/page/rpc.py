#!/usr/bin/env python
"""JSON RPC API root page."""

import inspect
import traceback
import umsgpack

from twisted.internet import defer
from twisted.web import resource
from twisted.web import server
from twisted import logger

from assembly_elements.cryptography import dsa
from assembly_elements.util import jsondate
from assembly_elements.web import auth
from assembly_elements.web.page import favicon

from assembly_filer import rpc

class DemoClass(object):
    @classmethod
    def echo(cls, text):
        return text

    @classmethod
    def sum(cls, *args):
        return sum(args)


class BaseRPC(resource.Resource):
    """A JsonRPC endpoint."""

    isLeaf = False
    logger = logger.Logger()
    ecdsa_auth = auth.AssemblyEcdsaAuth()

    def __init__(self, dsa_key_base_directory=None, dsa_key_leaf_directory=None):
        resource.Resource.__init__(self)
        self.verifier = dsa.Verifier(
            base_directory=dsa_key_base_directory, leaf_directory=dsa_key_leaf_directory
        )
        self.putChild(b"favicon.ico", favicon.get_favicon())
        self.request_parser = rpc.RpcRequestParser(concurrency=8)
        self.registered_methods = {}

    def add_demo_handlers(self):
        self.add_handler("democlass.echo", DemoClass.echo)
        self.add_handler("democlass.sum", DemoClass.sum)

    def add_handler(self, method_name, method):
        self.logger.error("add-handler")
        try:
            params = dict(inspect.signature(method).parameters)
            result = []
            for name, parameter in params.items():
                if parameter.default == inspect._empty:
                    result.append(name)
                else:
                    result.append("{}={}".format(name, parameter.default))
            signature = ", ".join(result)
            docstring = inspect.getdoc(method)
        except Exception as e:
            self.logger.error("failed to inspect method", method_name=method_name, method=str(method), e=str(e))
            signature = ""
            docstring = ""

        self.request_parser.register_method(method_name, method)
        self.registered_methods[method_name] = (signature, docstring)

    def encode_payload(self, *args, **kwargs):
        raise NotImplementedError(
            "required method encode_payload not implemented on class '{}'".format(
                self.__class__
            )
        )

    def decode_payload(self, *args, **kwargs):
        raise NotImplementedError(
            "required method decode_payload not implemented on class '{}'".format(
                self.__class__
            )
        )

    def render_token(self, request):
        request.setHeader("Content-Type", "application/msgpack")
        username = request.args.get(b"token")[0]
        """
        GENERATE KEYS
        """
        try:
            """
            Create and/or cache the keys for this user
            """
            self.verifier.keyfile_handler.get_or_create_verifying_key(username)
        except Exception:
            self.logger.failure("failed to get verifying key")

        token_df = self.ecdsa_auth.request_token(username.decode("utf-8"))
        token_df.addErrback(self.log_error, "request_token")

        session_df = request.getSharedSession()
        session_df.addErrback(self.log_error, "get_shared_session")

        df = defer.gatherResults([token_df, session_df])
        df.addCallback(self._render_token, request)
        df.addErrback(self.log_error, "gather_results")

        return server.NOT_DONE_YET

    def _render_token(self, result, request):
        token, session = result
        rval = {"data": token}
        try:
            outdata = self.encode_payload(rval)
        except Exception as e:
            self.logger.error("failed to self.encode_payload: {}".format(e))
            return self.render_error(
                request, error="failed to self.encode_payload the result"
            )

        self.logger.error("outdata", typ=str(type(outdata)))
        try:
            request.write(outdata)
        except Exception as e:
            self.logger.error(
                "failed to request.write(outdata)",
                e=str(e),
                outdata_type=str(type(outdata)),
            )
            raise
        request.finish()

    def render_GET(self, request):
        """Display a list of available methods."""
        self.logger.debug("render_GET")
        self.logger.error("request args", args=str(request.args))
        if b"token" in request.args:
            return self.render_token(request)
        request.setHeader("Content-Type", "text/html; charset=utf-8")
        table_string = "No methods registered"
        if self.registered_methods:
            table_data = ["<table>"]
            for method_name, (signature, docstring) in self.registered_methods.items():
                table_data.append("        <tr>")
                table_data.append("            <td>")
                table_data.append("                {}".format(method_name))
                table_data.append("            </td>")
                table_data.append("            <td>")
                table_data.append("                {}".format(signature))
                table_data.append("            </td>")
                table_data.append("            <td>")
                table_data.append("                {}".format(docstring))
                table_data.append("            </td>")
                table_data.append("        </tr>")
            table_data.append("</table>")
            table_string = "\n".join(table_data)

        self.logger.debug(table_string)

        rval = (
            "<html><head><title>Available Methods</title></head><body>"
            "<h1>Methods that are available at this location.</h1>"
            "{}"
            "</body></html>"
        )
        rval = rval.format(table_string)
        return rval.encode("utf-8")

    def render_error(self, request, **kwargs):
        rval = {"error": "unknown error"}
        rval.update(kwargs)
        request.write(self.encode_payload(rval))
        request.finish()
        return

    def render_POST(self, request):
        request.setHeader("Content-Type", "application/json")

        try:
            session_df = request.getSharedSession()
            session_df.addCallback(self.render_POST_SESSION)
            session_df.addErrback(self.log_error, "render_POST")
        except Exception as e:
            self.logger.error("session error", e=str(e))
            raise

        return server.NOT_DONE_YET

    def render_POST_SESSION(self, request):
        session = request.assembly_session
        content = request.content.getvalue()
        username = request.getHeader("x-assembly-username")
        signature = request.getHeader("x-assembly-signature")
        token = request.getHeader("x-assembly-token")
        if token:
            token = token.encode("utf-8")

        if session.username:
            if session.username != username:
                self.logger.error(
                    "username mismatch",
                    username=username,
                    session_username=session.username,
                )
                return self.render_error(request, error="username mismatch")
            # The user has already authenticated - we don't need to check the signature
            return self.process_request(request, username, content)

        if signature is None:
            self.logger.error("no signature and no session - must reauthenticate")
            return self.render_error(request, error="must reauthenticate")

        try:
            self.verifier.verify_signature(username, token+content, signature)
            session_attributes = {}
            cookie_update_df = session.authenticate(username, session_attributes)
            cookie_update_df.addCallback(session.factory.update_cookie, request)
            cookie_update_df.addCallback(lambda x:self.process_request(request, username, content))
            cookie_update_df.addErrback(self.log_error, "cookie update failed")
            return cookie_update_df
        except Exception as e:
            self.logger.error("bad signature", e=str(e))
            return self.render_error(request, error="bad signature")

    def process_request(self, request, username, content):
        self.logger.error("process_request: {}".format(username))

        df = self.request_parser.onMessage(username, content, isBinary=False)
        df.addCallback(self.write_and_finish, request)
        df.addErrback(self.log_error, "method_calling_failed")
        return df

    def write_and_finish(self, body, request):
        self.logger.error("write and finish", body=body, type=str(type(body)), request=request, finished=request.finished)
        request.write(body)
        request.finish()

    def _render_result(self, query_data, request, username):
        """Write some HTML to the client, close and finish."""
        self.logger.debug("_render_result", query_data=query_data)

        rval = {"result": query_data}
        try:
            outdata = self.encode_payload(rval)
        except Exception as e:
            self.logger.error("failed to self.encode_payload", e=str(e))
            rval.pop("data")
            return self.render_error(
                request,
                error="failed to self.encode_payload the result: {}  ::  {}".format(
                    e, rval
                ),
            )

        request.write(outdata)
        request.finish()

    def log_error(self, f, msg=None):
        """Log the traceback for any failure. Don't trap."""
        if not msg:
            msg = "unknown error"
        self.logger.error("ERROR: {}".format(msg), trace=f.getBriefTraceback())
        return f

    def getChild(self, name, request):
        return self


class JsonRPC(BaseRPC):
    def encode_payload(self, *args, **kwargs):
        return jsondate.dumps(*args, **kwargs).encode("utf-8")

    def decode_payload(self, *args, **kwargs):
        return jsondate.loads(*args, **kwargs)


class MsgpackRPC(BaseRPC):
    def encode_payload(self, *args, **kwargs):
        return umsgpack.packb(*args, **kwargs)

    def decode_payload(self, *args, **kwargs):
        return umsgpack.unpackb(*args, **kwargs)
