#!/usr/bin/env python

import traceback
import uuid

from twisted import logger
from twisted.web import server
from twisted.web import resource
from twisted.internet import defer

from assembly_elements.web import auth


class LoginLogout(resource.Resource):

    auth_provider = auth.AssemblyLdapAuth
    logger = logger.Logger()

    def render_POST(self, request):
        """Process a HTTP POST request."""
        self.logger.error("request args", args=request.args.keys())
        step = request.args[b"step"][0].decode("utf-8")

        if step == "logout":
            method = self.logout
        elif step == "authenticate":
            method = self.authenticate
        elif step == "request_password":
            method = self.request_password

        session_df = request.getSharedSession()
        session_df.addCallback(method)
        session_df.addErrback(self.log_error)
        return server.NOT_DONE_YET

    def authenticate(self, request):
        """Check the user-supplied credentials with auth_provider."""
        username = request.args[b"username"][0].decode("utf-8")
        password = request.args[b"password"][0].decode("utf-8")
        token = request.args[b"token"][0].decode("utf-8")

        authenticate_df = self.auth_provider.authenticate(username, password, token)
        authenticate_df.addCallback(self.authenticate_result, request, username)
        authenticate_df.addErrback(self.log_error)

        return server.NOT_DONE_YET

    def authenticate_result(self, result, request, username):
        """Update the session if the credentials are approved by auth_provider."""
        self.logger.debug("authenticate_result", result=result, username=username)
        if not result:
            return self.render_authentication_failed(request, username)

        session_attributes = {}

        """
        session.authenticate returns a deferred, but in this case we don't have
        to wait for its result.
        """
        session = request.assembly_session
        session_df = session.authenticate(username, session_attributes)
        session_df.addCallback(self._update_session_cookie, request)
        session_df.addErrback(self.log_error)

        return session_df

    def _update_session_cookie(self, session, request):
        """On authenticate, the cookie must be updated with a new session ID."""
        session.factory.update_cookie(session, request)
        return self.render_authentication_succeeded(request)

    def render_authentication_succeeded(self, request):
        body = ["""Authentication succeeded."""]
        return self.send_response("Authentication succeeded.", body, request)

    def logout(self, request):
        df = request.assembly_session.expire()
        df.addCallback(self.logout_ok, request)
        df.addErrback(self.log_error)

    def logout_ok(self, result, request):
        self.logger.debug("logout_ok: {}".format(result))
        body = ["""Logout succeeded."""]
        return self.send_response("Logout succeeded.", body, request)

    def request_password(self, request):
        """Display a form requesting a password."""
        username = request.args[b"username"][0].decode("utf-8")

        token_df = self.auth_provider.request_token(username)
        token_df.addCallback(self.render_login_page, request, username)
        token_df.addErrback(self.log_error)

        return server.NOT_DONE_YET

    def render_GET(self, request):
        """Get the session for this request and hand off to _render_GET."""

        session_df = request.getSharedSession()
        session_df.addCallback(self._render_GET)
        session_df.addErrback(self.log_error)

        return server.NOT_DONE_YET

    def send_response(self, title, body, request):
        """Return text - normally HTML - to the client browser."""
        # Compose a list of lines of text
        lines = self.get_html_header(title)
        lines.extend(body)
        lines.extend(self.get_html_footer())

        # Join
        text = "\n".join(lines)

        # Encode, write and finish
        request.write(text.encode("utf-8"))
        request.finish()

    def get_html_header(self, title):
        return ["""<html><head><title>""", title, """</title></head><body>"""]

    def get_html_footer(self):
        return ["""</body></html>"""]

    def render_authentication_failed(self, request, username):
        """Notify the user that authentication has failed."""
        body = ["""Authentication failed."""]
        return self.send_response("Authentication failed.", body, request)

    def render_username_page(self, request):
        """Ask the user for a username."""
        body = [
            """<form method="post">""",
            """<input type="hidden" name="step" value="request_password">""",
            """<label>Username:</label>""",
            """<input type="text" name="username" value="">""",
            """<input class="send_button" type="submit" value="Send">""",
            """</form>""",
        ]
        return self.send_response("Login", body, request)

    def render_login_page(self, token, request, username):
        """Ask the user for a password."""
        body = [
            """<form method="post">""",
            """<input type="hidden" name="step" value="authenticate">""",
            """<input type="hidden" name="token" value="{}">""".format(token),
            """<input type="hidden" name="username" value="{}">""".format(username),
            """<label>Password:</label>""",
            """<input type="password" name="password" value="">""",
            """<input class="login_button" type="submit" value="Login">""",
            """</form>""",
        ]
        return self.send_response("Login: enter password", body, request)

    def render_logout_page(self, request):
        """The user is logged in. Offer her a logout option."""
        body = [
            """<form method="post">""",
            """<input type="hidden" name="step" value="logout">""",
            """<input class="logout_button" type="submit" value="Logout">""",
            """</form>""",
        ]
        return self.send_response("Logout", body, request)

    def _render_GET(self, request):
        """Write some HTML to the client, close and finish."""
        session = request.assembly_session

        username = 0
        try:
            username = request.assembly_session.username
        except Exception as e:
            self.logger.error("_render_GET.exception: {}".format(e))

        self.logger.error("_render_GET.username: {}".format(username))

        if session.username is not None:
            return self.render_logout_page(request)
        else:
            return self.render_username_page(request)

    def log_error(self, f):
        """Log the traceback for any failure. Don't trap."""
        traceback = f.getBriefTraceback()
        self.logger.error("unexpected error: {}".format(traceback))
        return f
