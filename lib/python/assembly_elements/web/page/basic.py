#!/usr/bin/env python
"""Basic web page that uses the custom session code."""

import os

from assembly_elements.web import websocketrpc
from assembly_elements.web import ws
from assembly_elements.web import auth

from . import authenticate

from twisted import logger
from twisted.web import resource
from twisted.web import server
from twisted.web import static
from twisted.internet import defer

ASSEMBLY_ELEMENTS_BASEDIR = os.environ.get("ASSEMBLY_ELEMENTS_BASEDIR")


def get_favicon():
    """Return the bytes of the favicon file for efficient despatch."""
    ASSEMBLY_ELEMENTS_BASEDIR = os.environ.get("ASSEMBLY_ELEMENTS_BASEDIR")
    favicon = os.path.join(ASSEMBLY_ELEMENTS_BASEDIR, "static/img/favicon/favicon.ico")
    if not os.path.exists(favicon):
        raise Exception("Security Warning: Attempt to serve unknown static directory")

    favicon_data = b""
    with open(favicon, "rb") as fh:
        favicon_data = fh.read()
    return favicon_data


class DictionaryAuthenticator(auth.BaseAuth):
    """
    A very simple authenticator implementation for testing only.
    """

    tokens = {}
    token_expires = 90  # Auth tokens are valid for 90 seconds
    logger = logger.Logger()

    credentials = {"george":"michael", "emma":"goldman"}

    @classmethod
    def authenticate(cls, username, password, token):
        """Check the user's supplied credentials."""
        if cls.credentials.get(username) == password:
            return defer.succeed(True)
        return defer.succeed(False)


class Basic(resource.Resource):
    """A basic webpage that gets a session and displays its ID in the HTML."""

    isLeaf = False
    logger = logger.Logger()

    def __init__(self, *args, **kwargs):
        """Constructor."""
        port = kwargs.pop("port", 8092)
        resource.Resource.__init__(self, *args, **kwargs)
        html_resource = static.File(
            os.path.join(ASSEMBLY_ELEMENTS_BASEDIR, "static/html"), defaultType="text/html"
        )
        html_resource.contentEncodings[".html"] = "utf-8"
        self.putChild(b"html", html_resource)
        self.putChild(b"img", static.File(os.path.join(ASSEMBLY_ELEMENTS_BASEDIR, "static/img")))
        self.putChild(b"css", static.File(os.path.join(ASSEMBLY_ELEMENTS_BASEDIR, "static/css")))
        self.putChild(b"js", static.File(os.path.join(ASSEMBLY_ELEMENTS_BASEDIR, "static/js")))

        auth_page = authenticate.LoginLogout()
        auth_page.auth_provider = DictionaryAuthenticator
        self.putChild(b"login", auth_page)

        # The favicon.ico is maintained in memory as raw bytes
        self.putChild(b"favicon.ico", static.Data(get_favicon(), "image/x-icon"))

        # Add the websocket resource at /ws
        self.putChild(b"ws", ws.AssemblyWebSocketServerResource())

        # Is this secure?
        self.putChild(b"wsrpc", websocketrpc.get_resource(secure=False, port=port))

    def render_GET(self, request):
        """Get the session for this request and hand off to _render_GET."""
        request.setHeader("Content-Type", "text/html; charset=utf-8")
        self.logger.error("render_GET")
        session_df = request.getSharedSession()
        session_df.addCallback(self._render_GET)
        session_df.addErrback(self.log_error)
        return server.NOT_DONE_YET

    def _render_GET(self, request):
        """Write some HTML to the client, close and finish."""
        session = request.assembly_session
        self.logger.error("_render_GET: {}".format(session))
        self.logger.error("_render_GET: {}".format(session.session_id))

        session.attributes.setdefault("counter", 0)
        session.attributes["counter"] += 1
        session.update()

        request.write(
            u"""<html>\n    <head>
        <title>TITLE</title>
        <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-touch-icon.png" />
        <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png" />
        <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png" />
        <link rel="manifest" href="/assets/site.webmanifest">\n    </head>\n    """
            "<body>assembly_elements.tac: {} :: {}</body>\n</html>".format(
                session.session_id, session.attributes
            ).encode("utf-8")
        )
        request.finish()

    def log_error(self, f):
        """Log the traceback for any failure. Don't trap."""
        traceback = f.getBriefTraceback()
        self.logger.error("unexpected error: {}".format(traceback))
        return f

    def getChild(self, name, request):
        """Respond directly to request paths with no matching putChild assignment."""
        return self
