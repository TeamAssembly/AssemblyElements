#!/usr/bin/env python
"""A custom HTTP request object that manages its own session data."""

from assembly_elements.web.session import factory

from twisted import logger
from twisted.web.server import Request


class AssemblyRequest(Request):
    """
    The standard Request object does not return a deferred from getSession.

    This implementation returns a deferred from getSession. It's not how
    sessions are designed to work in twisted web, but it's simple and direct.
    """

    logger = logger.Logger()
    session_factory = factory.SessionFactory()

    def __init__(self, *args, **kwargs):
        """Constructor."""
        Request.__init__(self, *args, **kwargs)
        self._assembly_session_parameters = None
        self._assembly_session = {}

    # ---------------------------------------------------------------------------------

    @property
    def assembly_session(self):
        """Return the assembly_session attribute."""
        try:
            return self._assembly_session.get(
                self._assembly_session_parameters.is_secure
            )
        except AttributeError:
            return None

    @assembly_session.setter
    def assembly_session(self):
        """Raise an exception if the assembly_session attribute is set directly."""
        raise NotImplementedError("cannot set assembly_session attribute directly")

    @assembly_session.deleter
    def assembly_session(self):
        """Raise an exception if the assembly_session attribute is deleted."""
        raise NotImplementedError("cannot delete assembly_session attribute")

    def set_assembly_session(self, session):
        """Setter for self.assembly_session."""
        is_secure = self._assembly_session_parameters.is_secure
        self._assembly_session[is_secure] = session

    # ---------------------------------------------------------------------------------

    @property
    def assembly_session_parameters(self):
        """Return the assembly_session_parameters attribute."""
        return self._assembly_session_parameters

    @assembly_session_parameters.setter
    def assembly_session_parameters(self):
        """Raise an exception if the assembly_session_parameters attribute is set."""
        raise NotImplementedError(
            "cannot set assembly_session attribute directly - "
            "use set_assembly_session_parameters"
        )

    @assembly_session_parameters.deleter
    def assembly_session_parameters(self):
        """Raise an exception if the assembly_session_paramters attribute is deleted."""
        raise NotImplementedError("cannot delete assembly_session attribute")

    # ---------------------------------------------------------------------------------

    def set_assembly_session_parameters(self, parameters):
        """Set the parameters (cookie_name, is_secure) for this request's session."""
        self._assembly_session_parameters = parameters

    def log_error(self, f, label="logging_errback"):
        """Log an error without trapping it."""
        self.logger.error(label, msg=f.getBriefTraceback())
        return f

    def getSharedSession(self):
        """Return the session for the request."""
        return self.session_factory.getSharedSession(self)
