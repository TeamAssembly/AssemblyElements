#!/usr/bin/env python
"""Logging for Twisted Python (Probably Qt5Reactor) client programs."""

import datetime
import getpass
import grp
import inspect
import io
import json
import os
import platform
import socket
import sys

from twisted import logger

from . import config


class RebelLog(object):
    """Logging for Twisted Python (Probably Qt5Reactor) client programs."""

    _kw = None
    _observer = None
    _unpersistable = {"unpersistable": True}
    _local_vars = None
    _record_template = "\x1e{}\n"
    _config_filepath = None

    @classmethod
    def initialize(
        cls,
        level_name="info",
        basedir=None,
        prefix=None,
        suffix=None,
        filename=None,
        groupname=None,
    ):
        """Return an instance of RebelLog from this factory method.

        Args:
            level_name (str): Log level name.
            basedir (str): Base directory for log files.
            prefix (str): Log directory prefix, for e.g. "{year}/{month:02d}/{day:02d}/"
            suffix (str): Log directory suffix, for e.g. "{hostname}/{script}"
            filename (str): Log filename, for e.g. "{timestamp}.{pid}.log"
            groupname (str): Name of the group to own the shared directories we create.

        Returns:
            RebelLog: A RebelLog instance.
        """
        if all((level_name, basedir, prefix, suffix, filename, groupname)):
            cls._observer = cls(
                level_name=level_name,
                basedir=basedir,
                prefix=prefix,
                suffix=suffix,
                filename=filename,
                groupname=groupname,
            )
            return cls._observer

        return cls.initialize_from_config_filepath()

    @classmethod
    def initialize_from_config_filepath(cls, config_filepath=None):
        """Return an instance of RebelLog from this factory method.

        Args:
            config_filepath (str): Path to a config file with the required values.

        Raises:
            RuntimeError: If the filepath doesn't exist.

        Returns:
            RebelLog: A RebelLog instance.
        """
        if cls._observer is not None:
            return cls._observer

        if config_filepath is None:
            basedir = os.environ.get("ASSEMBLY_ELEMENTS_BASEDIR")
            config_filepath = os.path.join(basedir, "config/assembly_client_log.json")

        if not os.path.exists(config_filepath):
            raise RuntimeError(
                "Unable to find config file at '{}'".format(config_filepath)
            )

        config_object = config.get(config_filepath)
        client_log = config_object.get("client_log", {})
        pathdata = client_log.get("path", {})

        log_path_basedir = pathdata.get("basedir")
        log_path_prefix = pathdata.get("prefix")
        log_path_suffix = pathdata.get("suffix")
        log_path_filename = pathdata.get("filename")

        group = client_log.get("group")
        level = client_log.get("level", "info")

        cls._observer = cls(
            level_name=level,
            basedir=log_path_basedir,
            prefix=log_path_prefix,
            suffix=log_path_suffix,
            filename=log_path_filename,
            groupname=group,
        )
        return cls._observer

    def __init__(
        self,
        level_name="info",
        basedir=None,
        prefix=None,
        suffix=None,
        filename=None,
        groupname=None,
    ):
        """
        Initialize an instance of RebelLog.

        Args:
            level_name (str): Log level name.
            basedir (str): Base directory for log files.
            prefix (str): Log directory prefix, for e.g. "{year}/{month:02d}/{day:02d}/"
            suffix (str): Log directory suffix, for e.g. "{hostname}/{script}"
            filename (str): Log filename, for e.g. "{timestamp}.{pid}.log"
            groupname (str): Name of the group to own the shared directories we create.
        """
        self.level_name = level_name
        self.log_path = self.get_log_path(basedir, prefix, suffix, filename, groupname)
        log_stream = io.open(self.log_path, "a")

        observer = logger.FileLogObserver(log_stream, self.eventAsJSON)

        log_level = logger.LogLevel.levelWithName(level_name)
        predicate = logger.LogLevelFilterPredicate(defaultLogLevel=log_level)
        observer = logger.FilteringLogObserver(observer, [predicate])
        observer._encoding = "utf-8"

        # Prepare keyword arguments for json.dumps
        self._kw = dict(default=self._default, skipkeys=True)

        logger.globalLogPublisher.addObserver(observer)
        self.observer = observer
        _logger = logger.Logger()
        _logger.info("start logging", local_variables=self.get_local_variables())

    def get_local_variables(self):
        """Return a dict of variables from the current environment.

        Returns:
            dict: A dict of values we want to log.
        """
        if self._local_vars is not None:
            return self._local_vars

        try:
            uid = os.getuid()
            euid = os.geteuid()
        except Exception:
            uid = None
            euid = None

        try:
            with open("/proc/cpuinfo") as fh:
                cpuinfo = fh.readlines()
            for line in cpuinfo:
                if "model name" in line:
                    cpu = line.split(":", 1)[1].strip()
                    break
        except Exception:
            cpu = platform.processor()

        self.__class__._local_vars = {
            "hostname": socket.gethostname(),
            "pid": os.getpid(),
            "uid": uid,
            "euid": euid,
            "cpu": cpu,
            "uname": platform.uname(),
            "environ": dict(os.environ),
            "username": getpass.getuser(),
            "argv": sys.argv[:],
            "script": os.path.basename(inspect.stack()[-1][1]),
            "platform": platform.platform(),
            "cwd": os.getcwd(),
            "python": {
                "version": platform.python_version(),
                "interpreter": platform.python_implementation(),
            },
        }
        return self._local_vars

    def get_time_and_date(self):
        """Return a dict of time/date information.

        Returns:
            dict: A dict of time/date information.
        """
        now = datetime.datetime.now()
        return {
            "year": now.year,
            "month": now.month,
            "day": now.day,
            "timestamp": now.timestamp(),
        }

    def _default(self, unencodable):
        """
        Serialize an object not otherwise serializable by L{dumps}.

        Args:
            unencodable (object): An unencodable object.

        Returns:
            object: An encodable representation of the object.
        """
        if isinstance(unencodable, bytes):
            return unencodable.decode("utf-8")

        if isinstance(unencodable, datetime.datetime):
            return {"_timestamp": (unencodable - datetime.EPOCH).total_seconds()}

        result_dict = logger._json.objectSaveHook(unencodable)
        if result_dict == self._unpersistable:
            try:
                result_dict["object"] = str(unencodable)
            except Exception:
                pass

            try:
                result_dict["object_type"] = str(type(unencodable))
            except Exception:
                pass

        return result_dict

    def eventAsJSON(self, event):
        """
        Return a JSON-encoded log event.

        Encode an event as JSON, flattening it if necessary to preserve as much
        structure as possible.

        Not all structure from the log event will be preserved when it is
        serialized.

        Args:
            event (dict): A log event dictionary.

        Returns:
            str: A string of the serialized JSON terminated with a newline
        """
        if "assembly_log_source" in event:
            event["log_source"] = event.pop("assembly_log_source")
        else:
            try:
                frames = inspect.getouterframes(inspect.currentframe())
                frame = inspect.getframeinfo(frames[6][0])
                event["log_source"] = {
                    "filename": frame.filename, "lineno": frame.lineno
                }
            except Exception:
                event["log_source"] = {"filename": -1, "lineno": -1}

        if event["log_format"] == "{log_text}":
            event["log_format"] = event.pop("log_text", "")
        elif event["log_format"] == "{log_io}":
            event["log_format"] = event.pop("log_io", "")

        try:
            event["log_level"] = event["log_level"].name
        except Exception:
            pass

        event.pop("log_logger", None)
        event.pop("log_system", None)
        event.pop("log_flattened", None)

        logger._flatten.flattenEvent(event)
        result = json.dumps(event, **self._kw)

        if not isinstance(result, str):
            result = str(result, "utf-8", "replace")

        return self._record_template.format(result)

    def get_base_filepath(self, filepath):
        """Return the prefix of a filepath, and any remaining path fragments as a list.

        Example:
            Assuming that /var/log/some_client exists:

            Input: /var/log/some_client/create/these/extra/directories

            Result: "/var/log/some_client", ("create", "these", "extra", "directories")

        Raises:
            RuntimeError: If the filepath is not an absolute path.

        Args:
            filepath (str): A filepath.

        Returns:
            That prefix of the filepath that already exists and the remainder as a list.
        """
        if filepath[0] != os.sep:
            raise RuntimeError(
                "not an absolute path: {}".format(filepath), 103
            )

        head = filepath
        tails = []
        while True:
            if not head:
                msg = "failed to find base filepath of {}"
                raise RuntimeError(msg.format(filepath), 104)

            if os.path.exists(head):
                basepath = head
                tails = list(reversed(tails))
                return basepath, tails

            head, tail = head.rsplit(os.sep, 1)
            tails.append(tail)

    def get_log_path(self, basedir, prefix, suffix, filename, groupname):
        """Return the path to the logfile.

        Args:
            basedir (str): Base directory for log files.
            prefix (str): Log directory prefix, for e.g. "{year}/{month:02d}/{day:02d}/"
            suffix (str): Log directory suffix, for e.g. "{hostname}/{script}"
            filename (str): Log filename, for e.g. "{timestamp}.{pid}.log"
            groupname (str): Name of the group to own the shared directories we create.

        Returns:
            str: Path to the logfile.
        """
        local_variables = self.get_local_variables()

        dynamic_values = self.get_time_and_date()
        local_variables = self.get_local_variables()
        dynamic_values.update(local_variables)

        shared_log_path = os.path.join(basedir, prefix)
        shared_log_path = shared_log_path.format(**dynamic_values)
        basepath, tails = self.get_base_filepath(shared_log_path)

        uid = -1
        gid = grp.getgrnam(groupname).gr_gid

        for tail in tails:
            basepath = os.path.join(basepath, tail)
            os.makedirs(basepath, mode=0o770, exist_ok=True)
            os.chown(basepath, uid, gid, follow_symlinks=False)

        userdir = os.path.join(shared_log_path, local_variables["username"], suffix)
        userdir = userdir.format(**dynamic_values)

        basepath, tails = self.get_base_filepath(userdir)

        for tail in tails:
            basepath = os.path.join(basepath, tail)
            os.makedirs(basepath, mode=0o750, exist_ok=True)
            os.chown(basepath, uid, gid, follow_symlinks=False)

        os.makedirs(userdir, mode=0o750, exist_ok=True)

        log_path = os.path.join(userdir, filename)
        log_path = log_path.format(**dynamic_values)

        """
        We have to touch the logfile before we can set permissions on it.
        """
        with open(log_path, "a") as fh:
            pass

        """
        Writeable only by the owner, readable only by the owner and group.
        """
        os.chmod(log_path, 0o640)
        return log_path


def initialize():
    """Initialize the logger."""
    RebelLog.initialize()
