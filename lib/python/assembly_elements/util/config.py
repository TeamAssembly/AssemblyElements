#!/usr/bin/env python
"""Parse a config file into a singleton."""

import copy
import json
import os


class Config(object):
    """~Immutable Config singleton."""

    def __init__(self, config_file):
        """Constructor."""
        self.config_file = config_file
        self._data = None
        self._contents = None
        self.refresh()

    def __get_data(self):
        """Return deepcopy of config data for self.data (property)."""
        return copy.deepcopy(self._data)

    def __set_data(self, value):
        """Raise an exception."""
        raise RuntimeError("Config.data property is not mutable")

    data = property(__get_data, __set_data)

    def refresh(self):
        """re-read the data in our config file."""
        try:
            with open(self.config_file, "r") as fh:
                self._contents = fh.read()
        except Exception as e:
            msg = "failed to open/read config file at '{}': {} with process euid: {}"
            print(msg.format(self.config_file, e, os.geteuid()))
            raise

        try:
            self._data = json.loads(self._contents)
        except Exception as e:
            msg = "failed to deserialize data from config file at '{}': {}"
            print(msg.format(self.config_file, e))
            raise

    def get(self, key, *args):
        """Return configured value for key."""
        if len(args) == 0:
            return copy.deepcopy(self._data[key])

        default = args[0]
        return copy.deepcopy(self._data.get(key, default))

    def __str__(self):
        """Return string representation."""
        return self._contents

    def __hash__(self):
        """Return unique hash."""
        return hash(self._contents)

    def __eq__(self, other):
        """Equals."""
        return self._contents == other._contents

    def __ne__(self, other):
        """Not equals."""
        return self._contents != other._contents


class ConfigRepo(object):
    """Parse a config file into a singleton."""

    configs = {}

    @classmethod
    def get(cls, path):
        """Return the configuration for a filepath."""
        if path not in cls.configs:
            cls.refresh(path)
        return cls.configs[path]

    @classmethod
    def refresh(cls, path):
        """Refresh the config for a path."""
        cls.configs[path] = Config(path)
        return cls.configs[path]


def get(path):
    """Return the Config instance for the given path."""
    return ConfigRepo.get(path)
