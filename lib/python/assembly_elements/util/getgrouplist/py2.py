#!/bin/env python
"""Return the list of groups a user (uid or username) belongs to."""

import ctypes
from ctypes import util
import grp
import pwd
import six


class GetGroupListWrapper(object):
    """Wrap the libc getgrouplist call, which is not provided by python2."""

    libcfunc = None
    default_group_count = 100

    @classmethod
    def setup(cls):
        """Get getgrouplist from libc and configure its arguments."""
        if cls.libcfunc is not None:
            return

        libc = ctypes.cdll.LoadLibrary(util.find_library("libc"))
        libcfunc = libc.getgrouplist
        libcfunc.argtypes = [
            ctypes.c_char_p,
            ctypes.c_uint,
            ctypes.POINTER(ctypes.c_uint * cls.default_group_count),
            ctypes.POINTER(ctypes.c_int),
        ]
        libcfunc.restype = ctypes.c_int32
        cls.libcfunc = libcfunc

    @classmethod
    def getgrouplist(cls, uid, ngroups=None):
        """Return the list of group names for the given user."""
        if isinstance(uid, bytes):
            uid = uid.decode("utf-8")

        if isinstance(uid, (str, six.text_type)):
            username = uid
            user = pwd.getpwnam(username)
            uid = user.pw_uid
            gid = user.pw_gid
        else:
            user = pwd.getpwuid(uid)
            username = user.pw_name
            gid = user.pw_gid

        cls.setup()

        if ngroups is None:
            ngroups = cls.default_group_count

        grouplist = (ctypes.c_uint * ngroups)()
        ngrouplist = ctypes.c_int(ngroups)
        cls.libcfunc.argtypes[2] = ctypes.POINTER(ctypes.c_uint * ngroups)

        count = cls.libcfunc(
            username.encode("utf-8"),
            gid,
            ctypes.byref(grouplist),
            ctypes.byref(ngrouplist),
        )
        if count == -1:
            """
            If there are more than 'cls.default_group_count' results,
            'count' will be -1
            The correct number of groups is now in 'ngrouplist.value'
            """
            return cls.getgrouplist(uid, ngroups=int(ngrouplist.value))

        return [grp.getgrgid(gid).gr_name for gid in grouplist[0:count]]


getgrouplist = GetGroupListWrapper.getgrouplist


if __name__ == "__main__":
    print(getgrouplist(1000))
    print("")
    print(getgrouplist(b"donal"))
    print("")
    print(getgrouplist("donal"))
