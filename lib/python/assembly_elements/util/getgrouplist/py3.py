#!/bin/env python
"""Return the list of groups a user (uid or username) belongs to."""

import grp
import os
import pwd


def getgrouplist(uid, ngroups=None):
    """Return the list of group names for the given user."""
    if isinstance(uid, bytes):
        uid = uid.decode("utf-8")

    if isinstance(uid, str):
        uid = pwd.getpwnam(uid).pw_uid

    user = pwd.getpwuid(uid)
    g = os.getgrouplist(user.pw_name, user.pw_gid)
    return [grp.getgrgid(gid).gr_name for gid in g]


if __name__ == "__main__":
    print(getgrouplist(1000))
    print("")
    print(getgrouplist(b"donal"))
    print("")
    print(getgrouplist("donal"))
