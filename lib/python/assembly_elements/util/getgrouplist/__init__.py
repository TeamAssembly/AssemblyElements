#!/bin/env python

import platform
import sys

import six


if 'linux' == platform.system().lower():
    if six.PY2:
        from .py2 import getgrouplist  # noqa
    elif sys.implementation.name == "pypy" and sys.implementation.version.minor == 6:
        # pypy 3.6 doesn't have getgrouplist yet
        from .py2 import getgrouplist  # noqa
    else:
        from .py3 import getgrouplist  # noqa
else:
    def getgrouplist(*args, **kwargs):
        return []
