#!/usr/bin/env python
"""Custom exceptions for AssemblyElements."""


class AssemblyException(Exception):
    """Generic Assembly Exception."""


class AssemblyRuntimeError(AssemblyException):
    """Generic Assembly Exception."""


class AssemblyEnvironmentError(AssemblyException):
    """Generic Assembly Exception."""


class AssemblyNotAuthenticatedError(AssemblyException):
    """No user has yet authenticated for a session."""


class AssemblyPermissionError(AssemblyException):
    """The authenticated user does not have the required permissions."""


class AssemblyWebSocketException(AssemblyException):
    """Websocket RPC call exception."""

    unexpected_error = 2
    no_such_method = 3
    unrecognised_token = 4
    expired_token = 5
    username_mismatch = 6
    bad_signature = 7
    bad_timestamp = 8

    def __init__(self, code, message=None):
        """
        Send an error with errno and message to a user over a remote connection.

        This Exception should never contain any sensitive information.
        """
        self.args = (code, message)
        self.errno = code
        self.message = message


class AssemblyWebSocketAuthenticationException(AssemblyWebSocketException):
    """Authentication failure in the websocket API."""
