#!/usr/bin/env python
"""Json encoder/decoder that understands Python's datetime objects."""

import json

from assembly_elements import datetime


def default(obj):
    """Encode datetime objects to JSON."""
    if isinstance(obj, datetime.datetime):
        return {"_timestamp": (obj - datetime.EPOCH).total_seconds()}
    return super().default(obj)


def object_hook(obj):
    """Decode datetime objects from JSON."""
    if "_timestamp" in obj:
        return datetime.utcfromtimestamp(obj["_timestamp"])
    return obj


def dumps(*args, **kwargs):
    """Serialize an object to a JSON string."""
    return json.dumps(*args, default=default, **kwargs)


def dump(*args, **kwargs):
    """Serialize an object to a JSON file."""
    return json.dump(*args, default=default, **kwargs)


def loads(*args, **kwargs):
    """Deserialize a JSON string."""
    return json.loads(*args, object_hook=object_hook, **kwargs)


def load(*args, **kwargs):
    """Deserialize a JSON file."""
    return json.load(*args, object_hook=object_hook, **kwargs)
