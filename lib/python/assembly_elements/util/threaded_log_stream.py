#!/usr/bin/env python3
"""Replace io.open in creating an object that handles .write calls in a thread."""

from twisted.internet import threads


class LogStream(object):
    """An object that handles .write calls in a thread."""

    def __init__(self, filepath):
        """Initialize an instance of LogStream.

        Args:
            filepath (str): The absolute path to the logfile.
        """
        # We have two queues:
        # - one that we add events to in the main thread
        # - one that we modify in the logwriting IO thread
        self.queues = ([], [])
        self.index = 0
        self.running = False

        self.filepath = filepath
        self.openLogFile()

    def initiateWrites(self):
        """Initiate the process of dumping log events to disk."""
        if self.running:
            return

        self.running = True

        index = self.index
        self.index = 0 if self.index else 1

        df = threads.deferToThread(self.logInThread, index)
        df.addCallback(self.checkRunState)
        return df

    def logInThread(self, index):
        """Write all the messages on the given queue to disk.

        Args:
            index (int): 1 or 0 - the index of the queue to write.
        """
        data = "".join(self.queues[index])
        self.queues[index].clear()
        self.writeEventStream(data.encode("utf-8"))

    def checkRunState(self, result):
        """When a sweep finishes, unset self.running & check for any pending messages.

        Args:
            result (None): Result of self.logInThread
        """
        self.running = False
        if self.queues[self.index]:
            self.initiateWrites()

    def writeEventStream(self, data, is_retry=False):
        """Write some data to the logfile.

        If the write fails on a 'write to closed file' error, try to re-open it.

        Args:
            data (bytes): Data to write to the log file.
            is_retry (bool): True if this write attempt is a retry.
        """
        try:
            self.fh.write(data)
            self.fh.flush()
        except Exception as e:
            if is_retry:
                return

            if "write to closed file" in e.args:
                self.openLogFile()
                self.writeEventStream(data, is_retry=True)

    def openLogFile(self):
        """Open the log file for writing."""
        self.fh = open(self.filepath, "ab")

    def write(self, jsondata):
        """Receive a log event (already serialized to JSON) from the application.

        Args:
            jsondata (bytes): A json-serialized event blob.
        """
        self.queues[self.index].append(jsondata)
        if self.running:
            return
        return self.initiateWrites()
