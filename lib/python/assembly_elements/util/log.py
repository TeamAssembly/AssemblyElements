#!/usr/bin/env pypy
"""A reasonable JSON logger for use with logstash."""

import getpass
import inspect
import io
import json
import socket
import traceback

from twisted import logger

from assembly_elements import datetime
from . import threaded_log_stream

HOSTNAME = socket.gethostname()
USERNAME = getpass.getuser()


class LogstashJsonFileLogObserver(object):
    """A reasonable JSON logger for use with logstash."""

    indent = None
    separators = (",", ":")
    # indent = 4
    # separators = (",", ": ")
    observers = {}
    kw = None
    _default_result_dict = {"unpersistable": True}
    recordPrefix = "\x1e"

    @classmethod
    def get_observer(cls, log_path, log_level_name="debug"):
        """Return the configured log observer for the given log path.

        Return an observer that does its own blocking File IO. If the target
        filesystem stops responding, this observer can freeze your program
        indefinitely.

        Args:
            log_path (str): Abolute path to write the log file to.
            log_level_name (str): Log level ike 'debug' or 'info'.

        Returns:
            logger.FilteringLogObserver instance.
        """
        if log_path not in cls.observers:
            log_stream = io.open(log_path, "a")
            return cls._get_observer(log_stream, log_path, log_level_name)

        return cls.observers[log_path]

    @classmethod
    def get_threaded_observer(cls, log_path, log_level_name="debug"):
        """Return the configured log observer for the given log path.

        Instead of writing directly to a file (blocking IO) this observer uses a
        threaded_log_stream.LogStream instance to write log entries through a
        non-blocking threading API.

        Args:
            log_path (str): Abolute path to write the log file to.
            log_level_name (str): Log level ike 'debug' or 'info'.

        Returns:
            logger.FilteringLogObserver instance.
        """
        if log_path not in cls.observers:
            log_stream = threaded_log_stream.LogStream(log_path)
            return cls._get_observer(log_stream, log_path, log_level_name)

        return cls.observers[log_path]

    @classmethod
    def _get_observer(cls, log_stream, log_path, log_level_name="debug"):
        """Return the configured log observer for the given log path.

        Args:
            log_stream (File): A file-like object with .write and .close methods.
            log_path (str): Abolute path to write the log file to.
            log_level_name (str): Log level ike 'debug' or 'info'.

        Returns:
            logger.FilteringLogObserver instance.
        """
        observer = logger._file.FileLogObserver(log_stream, cls.eventAsJSON)

        """
        Filter on log_level
        """
        log_level = logger.LogLevel.levelWithName(log_level_name)
        predicate = logger.LogLevelFilterPredicate(defaultLogLevel=log_level)
        observer = logger.FilteringLogObserver(observer, [predicate])
        observer._encoding = "utf-8"
        cls.observers[log_path] = observer

        return cls.observers[log_path]

    @classmethod
    def _default(cls, unencodable):
        """
        Serialize an object not otherwise serializable by L{dumps}.

        Args:
            unencodable (object): Any unencodable object.

        Returns:
            str: A serializeed unencodable
        """
        if isinstance(unencodable, bytes):
            return unencodable.decode("utf-8")

        if isinstance(unencodable, datetime.datetime):
            return {"_timestamp": (unencodable - datetime.EPOCH).total_seconds()}

        result_dict = logger._json.objectSaveHook(unencodable)
        if result_dict == cls._default_result_dict:
            try:
                result_dict["object"] = str(unencodable)
            except Exception:
                pass

            try:
                result_dict["object_type"] = str(type(unencodable))
            except Exception:
                pass

        return result_dict

    @classmethod
    def eventAsJSON(cls, event):
        """
        Return a JSON-encoded log event.

        Encode an event as JSON, flattening it if necessary to preserve as much
        structure as possible.

        Not all structure from the log event will be preserved when it is
        serialized.

        Args:
            event (dict):  A log event dictionary.

        Returns:
            str: A string of the serialized JSON.
        """
        # Avoid using inspect because it defeats the pypy JIT.
        lsp = event.pop("log_source_precomputed", None)
        if lsp:
            event["log_source"] = lsp
        else:
            try:
                frames = inspect.getouterframes(inspect.currentframe())
                frame = inspect.getframeinfo(frames[6][0])
                event["log_source"] = {
                    "filename": frame.filename,
                    "lineno": frame.lineno,
                }
            except Exception:
                event["log_source"] = {"filename": -1, "lineno": -1}

        if event["log_format"] == "{log_text}":
            event["log_format"] = event.pop("log_text", "")
        elif event["log_format"] == "{log_io}":
            event["log_format"] = event.pop("log_io", "")

        event.pop("log_logger", None)
        event.pop("log_system", None)
        event.pop("log_flattened", None)

        logger._flatten.flattenEvent(event)
        result = json.dumps(
            event,
            default=cls._default,
            skipkeys=True,
            indent=cls.indent,
            separators=cls.separators,
        )

        """
        Each log record is prefixed with the recordSeparator character and
        suffixed with a newline.
        """
        return result + cls.recordPrefix


def get_log_observer(config_data, **kwargs):
    """Configure and return a LogstashJsonFileLogObserver.

    Args:
        config_data (dict): Configuration data dict.
        kwargs (dict): Local configuration data dict.

    Returns:
        logger.FilteringLogObserver instance.
    """
    username = USERNAME
    hostname = HOSTNAME

    path_config_data = config_data.copy()
    path_config_data.update(kwargs)

    log_path = config_data["path"].format(
        log_username=username, log_hostname=hostname, **path_config_data
    )

    log_level_name = config_data.get("level", "debug")
    observer = LogstashJsonFileLogObserver.get_threaded_observer(
        log_path, log_level_name
    )

    return observer


def get_global_log_observer(config_data, **kwargs):
    """Instance and return the global log observer.

    Args:
        config_data (dict): Configuration data dict.
        kwargs (dict): Local configuration data dict.

    Returns:
        logger.FilteringLogObserver instance.
    """
    observer = get_log_observer(config_data, **kwargs)
    logger.globalLogPublisher.addObserver(observer)
    GlobalLogSingleton.addObserver(observer)
    return observer


class GlobalLogSingleton(object):
    """Singleton to manage the global log observer."""

    observed = False

    @classmethod
    def addObserver(cls, observer):
        """Add observer to logger.

        Args:
            observer (logger.FilteringLogObserver): Twisted log observer.

        Returns:
            logger.FilteringLogObserver instance.
        """
        if cls.observed:
            logger.globalLogPublisher.addObserver(observer)
        else:
            try:
                logger.globalLogBeginner.beginLoggingTo([observer])
            except Exception:
                print(traceback.format_exc())
        cls.observed = True
        return observer
