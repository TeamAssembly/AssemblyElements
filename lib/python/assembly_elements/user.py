#!/usr/bin/env python
"""Represent a UNIX user."""

from twisted.internet import reactor
from twisted.internet import threads

from assembly_elements.util import getgrouplist

class User(object):
    """Represent a UNIX user."""

    def __init__(self, username):
        """Constructor."""
        self.username = username

    def groups(self):
        """Return the list of UNIX groups for this user."""
        return thread.deferToThread(self.t_getgrouplist)

    def t_getgrouplist(self):
        """
        Return the list of UNIX groups for this user.

        This method has the potential to block and must be run in a thread.
        """
        return getgrouplist(self.username)
