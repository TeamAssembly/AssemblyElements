#!/usr/bin/env python

import os

ASSEMBLY_ELEMENTS_BASEDIR = os.environ.get("ASSEMBLY_ELEMENTS_BASEDIR")


class Util(object):

    data = None

    @classmethod
    def setup(cls):
        if cls.data is not None:
            return
        cls.refresh()

    @classmethod
    def refresh(cls):
        if cls.data is None:
            cls.data = {}
        cls.data["test_data"] = os.path.join(ASSEMBLY_ELEMENTS_BASEDIR, "test_data")

    @classmethod
    def get(cls, key, default):
        cls.setup()
        return cls.data.get(key, default)

    @classmethod
    def get_test_file(cls, *args):
        test_data = cls.get("test_data", None)
        return os.path.join(test_data, *args)
