#!/usr/bin/env python

import os
import json
from twisted.trial import unittest
from twisted import logger

from assembly_elements_tests import util

from assembly_elements.util import config
from assembly_elements.util import log

ASSEMBLY_ELEMENTS_BASEDIR = os.environ.get("ASSEMBLY_ELEMENTS_BASEDIR")


class LogTest(unittest.TestCase):
    def setUp(self):
        self.logger = logger.Logger()

    def test_global_logger(self):
        test_config_filepath = util.Util.get_test_file("config_file.json")
        appconfig = config.ConfigRepo.get(test_config_filepath)
        logconfig = appconfig.get("log", None)
        real_log_path = appconfig.get("real_log_path", None)
        assert not os.path.exists(real_log_path)

        log.get_global_log_observer(logconfig)
        assert os.path.exists(real_log_path)

        TEST_MESSAGE = "test log message"
        self.logger.error(TEST_MESSAGE)

        with open(real_log_path) as fh:
            lines = fh.readlines()

        assert json.loads(lines[-1])["log_format"] == TEST_MESSAGE

        os.unlink(real_log_path)

        assert appconfig.get("appname", None) == "AssemblyServer"
