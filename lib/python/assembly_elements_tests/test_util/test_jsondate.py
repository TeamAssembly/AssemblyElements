#!/usr/bin/env python

import os
from twisted.trial import unittest
from twisted import logger

from assembly_elements.util import jsondate
from assembly_elements import datetime

ASSEMBLY_ELEMENTS_BASEDIR = os.environ.get("ASSEMBLY_ELEMENTS_BASEDIR")


class JsonDateTest(unittest.TestCase):
    def setUp(self):
        self.logger = logger.Logger()

    def test_global_logger(self):

        testdate = datetime.utcnow()
        testblock = {"mydate":testdate}

        serialized = jsondate.dumps(testblock)
        deserialized = jsondate.loads(serialized)

        assert deserialized == testblock
