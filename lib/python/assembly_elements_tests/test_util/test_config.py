#!/usr/bin/env python

from twisted.trial import unittest

from assembly_elements_tests import util

from assembly_elements.util import config


class ConfigTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_get(self):
        test_config_filepath = util.Util.get_test_file("config_file.json")
        appconfig = config.ConfigRepo.get(test_config_filepath)
        assert appconfig.get("appname", None) == "AssemblyServer"
