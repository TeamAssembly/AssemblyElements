#!/usr/bin/env python
"""Test getgrouplist - get list of groups for user."""

from twisted.trial import unittest

from assembly_elements.util import getgrouplist


class GetGroupListTest(unittest.TestCase):

    def test_str(self):
        assert "root" in getgrouplist(0)

    def test_bytes(self):
        assert "root" in getgrouplist(b"root")

    def test_int(self):
        assert "root" in getgrouplist("root")
