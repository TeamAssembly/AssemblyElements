#!/usr/bin/env python

import six
import traceback
from twisted.trial import unittest
from twisted.internet import task
from twisted.internet import reactor
from twisted.internet import defer

from assembly_elements_tests import util

from assembly_elements.database import postgres
from assembly_elements.util import config
from assembly_elements.util.exceptions import AssemblyRuntimeError

operational_errors = []
try:
    import psycopg2cffi

    operational_errors.append(psycopg2cffi._impl.exceptions.OperationalError)
except Exception:
    pass

try:
    import psycopg2

    operational_errors.append(psycopg2.OperationalError)
except Exception:
    pass


if not operational_errors:
    raise Exception("missing both psycopg2 and psycopg2cffi")


def eb(f):
    print(f.getBriefTraceback())
    return f


class ConfigTest(unittest.TestCase):
    def test_connect(self):
        test_config_filepath = util.Util.get_test_file("database_config.json")
        database_config = config.ConfigRepo.get(test_config_filepath)
        pg_pool = postgres.Pool.get_instance_from_config(database_config)
        df = pg_pool.start()

        def stop(result):
            df = pg_pool.close()
            pg_pool.purge_cache()
            return df

        df.addCallback(stop)
        return df

    def test_double_initialize(self):
        postgres.Pool.initialize()
        assert postgres.Pool.ready == True
        postgres.Pool.initialize()

    def test_select(self):
        test_config_filepath = util.Util.get_test_file("database_config.json")
        database_config = config.ConfigRepo.get(test_config_filepath)
        pg_pool = postgres.Pool.get_instance_from_config(database_config)
        df = pg_pool.start()
        df.addCallback(self._test_select, pg_pool)
        return df

    def _test_select(self, result, pg_pool):
        query = """SELECT * FROM pg_catalog.pg_namespace WHERE nspname='pg_catalog'"""
        df = pg_pool.runQuery(query)

        def cb(result):
            assert result[0].nspname == "pg_catalog"
            df = pg_pool.close()
            pg_pool.purge_cache()
            return df

        df.addCallback(cb)
        return df

    def test_add_too_many_connections(self):
        test_config_filepath = util.Util.get_test_file("database_config.json")
        database_config = config.ConfigRepo.get(test_config_filepath)

        connections = 2
        pg_pool = postgres.Pool.get_instance_from_config(
            database_config, min_connections=connections, max_connections=connections
        )
        df = pg_pool.start()

        def add_connection(r):
            assert len(pg_pool.connections) == connections
            return pg_pool.addConnection()

        def catch_failure(r):
            r.trap(AssemblyRuntimeError)
            assert len(pg_pool.connections) == connections
            df = pg_pool.close()
            pg_pool.purge_cache()
            return df

        df.addCallback(add_connection)
        df.addErrback(catch_failure)
        return df

    def test_add_connection(self):
        test_config_filepath = util.Util.get_test_file("database_config.json")
        database_config = config.ConfigRepo.get(test_config_filepath)

        connections = 2
        pg_pool = postgres.Pool.get_instance_from_config(
            database_config,
            min_connections=connections,
            max_connections=connections + 10,
        )
        df = pg_pool.start()

        def add_connection(r):
            assert len(pg_pool.connections) == connections
            return pg_pool.addConnection()

        def count_connections(r):
            assert len(pg_pool.connections) == connections + 1
            df = pg_pool.close()
            pg_pool.purge_cache()
            return df

        df.addCallback(add_connection)
        df.addCallback(count_connections)
        return df

    def test_double_start(self):
        test_config_filepath = util.Util.get_test_file("database_config.json")
        database_config = config.ConfigRepo.get(test_config_filepath)

        def second_start(f):
            return pg_pool.start()

        def close_pool(f):
            df = pg_pool.close()
            pg_pool.purge_cache()
            return df

        pg_pool = postgres.Pool.get_instance_from_config(database_config)
        df = pg_pool.start()
        df.addCallback(second_start)
        df.addCallback(close_pool)
        return df

    def test_failed_connection(self):
        """
        This test inexplicably fails on python 2.7.
        """
        if six.PY2:
            return
        test_config_filepath = util.Util.get_test_file("database_config_bad.json")
        database_config = config.ConfigRepo.get(test_config_filepath)

        def extract_first_error(f):
            f.trap(defer.FirstError)
            return f.value.subFailure

        pg_pool = postgres.Pool.get_instance_from_config(database_config)
        df = pg_pool.start()
        df.addErrback(extract_first_error)
        self.assertFailure(df, *operational_errors)
        df.addBoth(lambda x: pg_pool.purge_cache())

        return df

    def test_bad_start_callback(self):
        """
        Adding a failing callback to the 'start' method should not bring down the
        connections nor the reactor.
        """
        test_config_filepath = util.Util.get_test_file("database_config.json")
        database_config = config.ConfigRepo.get(test_config_filepath)

        def raise_exception(result):
            raise Exception("Testing a bad callback")

        pg_pool = postgres.Pool.get_instance_from_config(database_config)
        df = pg_pool.start()
        df.addCallback(raise_exception)
        df.addBoth(lambda x: pg_pool.purge_cache())

        return df
