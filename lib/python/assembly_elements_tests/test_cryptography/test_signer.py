#!/usr/bin/env python

import os
import shutil
import tempfile

import ecdsa

from twisted.trial import unittest

from assembly_elements.cryptography import dsa

PERMISSION_DENIED = 13
NOT_PERMITTED = 1
FILE_EXISTS = 17


class SignerTest(unittest.TestCase):

    homedir = {"darwin": "/Users", "linux": "/home", "sunos": "/export/home"}
    OCT0750 = int("0750", 8)
    temp_directories = set()

    @classmethod
    def setUpClass(cls):
        cls.tempdir = tempfile.mkdtemp(suffix="_assembly_trial_tests")

    @classmethod
    def tearDownClass(cls):
        for d in SignerTest.temp_directories:
            try:
                shutil.rmtree(d)
            except Exception:
                pass

    def test_refresh_key(self):
        tempdir = self.make_temp_directory()
        signer = dsa.Signer(base_directory=tempdir)
        username = os.environ.get("USER")

        key = signer.refresh_key(username)
        assert isinstance(key, ecdsa.SigningKey)
        shutil.rmtree(tempdir)

    def test_get_signing_key(self):
        tempdir = self.make_temp_directory()
        signer = dsa.Signer(base_directory=tempdir)
        username = os.environ.get("USER")

        key = signer.get_signing_key(username)
        assert isinstance(key, ecdsa.SigningKey)
        shutil.rmtree(tempdir)

    def test_refresh_key_bad_username(self):
        tempdir = self.make_temp_directory()
        signer = dsa.Signer(base_directory=tempdir)
        username = "bad_user_name"

        try:
            signer.refresh_key(username)
        except Exception:
            pass
        finally:
            shutil.rmtree(tempdir)

    def test_sign_message(self):
        tempdir = self.make_temp_directory()
        signer = dsa.Signer(base_directory=tempdir)
        username = os.environ.get("USER")
        message = "Test message"

        signature = signer.sign_message(username, message)
        assert isinstance(signature, bytes)

        verifier = dsa.Verifier(base_directory=tempdir)
        verifier.verify_signature(username, message, signature)

        shutil.rmtree(tempdir)

    def make_temp_directory(self, username=None):
        tempdir = tempfile.mkdtemp(suffix="_assembly_trial_tests")
        SignerTest.temp_directories.add(tempdir)

        if username is None:
            username = os.environ.get("USER")

        kfh = dsa.KeyFileHandler(base_directory=tempdir)
        username = os.environ.get("USER")
        kfh.create_user_directory(username)

        return tempdir
