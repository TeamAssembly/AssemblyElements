#!/usr/bin/env python

import getpass
import os
import shutil
import tempfile

import ecdsa

from twisted.trial import unittest

from assembly_elements.cryptography import dsa

PERMISSION_DENIED = 13
NOT_PERMITTED = 1
FILE_EXISTS = 17


class VerifierTest(unittest.TestCase):

    homedir = {"darwin": "/Users", "linux": "/home", "sunos": "/export/home"}
    OCT0750 = int("0750", 8)
    temp_directories = set()

    @classmethod
    def setUpClass(cls):
        cls.tempdir = tempfile.mkdtemp(suffix="_assembly_trial_tests")

    @classmethod
    def tearDownClass(cls):
        for d in VerifierTest.temp_directories:
            try:
                shutil.rmtree(d)
            except Exception:
                pass

    def test_refresh_key(self):
        tempdir, _ = self.make_temp_directory()
        username = os.environ.get("USER")

        signer = dsa.Signer(base_directory=tempdir)
        signer.get_signing_key(username)

        verifier = dsa.Verifier(base_directory=tempdir)
        key = verifier.refresh_key(username)

        assert isinstance(key, ecdsa.VerifyingKey)
        shutil.rmtree(tempdir)

    def test_get_verifying_key(self):
        tempdir, _ = self.make_temp_directory()
        username = os.environ.get("USER")

        signer = dsa.Signer(base_directory=tempdir)
        signer.get_signing_key(username)

        verifier = dsa.Verifier(base_directory=tempdir)
        key = verifier.get_verifying_key(username)

        assert isinstance(key, ecdsa.VerifyingKey)
        shutil.rmtree(tempdir)

    def test_verify_signature(self):
        tempdir, _ = self.make_temp_directory()
        username = os.environ.get("USER")

        signer = dsa.Signer(base_directory=tempdir)
        signer.get_signing_key(username)

        verifier = dsa.Verifier(base_directory=tempdir)
        message = "Test message"

        signature = signer.sign_message(username, message)
        assert isinstance(signature, bytes)

        verifier = dsa.Verifier(base_directory=tempdir)
        verifier.verify_signature(username, message, signature)

        shutil.rmtree(tempdir)

    def test_verify_bad_signature(self):
        tempdir, kfh = self.make_temp_directory()
        username = os.environ.get("USER")

        signer = dsa.Signer(base_directory=tempdir)
        signer.get_signing_key(username)

        verifier = dsa.Verifier(base_directory=tempdir)

        first_message = "Test message"
        first_signature = signer.sign_message(username, first_message)
        assert isinstance(first_signature, bytes)
        verifier.verify_signature(username, first_message, first_signature)

        second_message = "Second test message"
        second_signature = signer.sign_message(username, second_message)
        assert isinstance(second_signature, bytes)
        verifier.verify_signature(username, second_message, second_signature)

        # The signing key is now cached. We must replace it.
        kfh.create_keypair(getpass.getuser())
        signer.refresh_key(username)
        signer.get_signing_key(username)

        # This should fail, but silently refresh the key and then succeed.
        third_message = "Third test message"
        third_signature = signer.sign_message(username, third_message)
        assert isinstance(third_signature, bytes)
        verifier.verify_signature(username, third_message, third_signature)

        shutil.rmtree(tempdir)

    def make_temp_directory(self, username=None):
        tempdir = tempfile.mkdtemp(suffix="_assembly_trial_tests")
        VerifierTest.temp_directories.add(tempdir)

        if username is None:
            username = os.environ.get("USER")

        kfh = dsa.KeyFileHandler(base_directory=tempdir)
        username = os.environ.get("USER")
        kfh.create_user_directory(username)

        return tempdir, kfh
