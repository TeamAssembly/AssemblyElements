#!/usr/bin/env python

import os
import pwd
import platform
import shutil
import stat
import tempfile

import ecdsa

from twisted.trial import unittest

from assembly_elements.util.exceptions import AssemblyEnvironmentError
from assembly_elements.util.exceptions import AssemblyRuntimeError


from assembly_elements.cryptography import dsa

PERMISSION_DENIED = 13
NOT_PERMITTED = 1
FILE_EXISTS = 17


class KeyTest(unittest.TestCase):

    homedir = {"darwin": "/Users", "linux": "/home", "sunos": "/export/home"}
    temp_directories = set()

    @classmethod
    def setUpClass(cls):
        cls.tempdir = tempfile.mkdtemp(suffix="_assembly_trial_tests")

    @classmethod
    def tearDownClass(cls):
        for d in KeyTest.temp_directories:
            try:
                shutil.rmtree(d)
            except Exception:
                pass

    def make_temp_directory(self, username=None):
        tempdir = tempfile.mkdtemp(suffix="_assembly_trial_tests")
        KeyTest.temp_directories.add(tempdir)

        if username is None:
            username = os.environ.get("USER")

        kfh = dsa.KeyFileHandler(base_directory=tempdir)
        username = os.environ.get("USER")
        kfh.create_user_directory(username)

        return tempdir

    def get_default_home_path(self):
        system = platform.system().lower()
        return self.homedir.get(system, "/home")

    def test_get_keyfile_directory_default(self):
        username = os.environ.get("USER")
        homedir_path = os.path.expanduser("~{0}".format(username))

        kfh = dsa.KeyFileHandler()
        expected = os.path.join(homedir_path, kfh.leaf_directory)
        result = kfh.get_keyfile_directory(username)

        assert result == expected

    def test_get_keyfile_directory(self):
        tempdir = self.make_temp_directory()

        kfh = dsa.KeyFileHandler(base_directory=tempdir)
        username = os.environ.get("USER")
        expected = os.path.join(tempdir, username, kfh.leaf_directory)
        result = kfh.get_keyfile_directory(username)

        shutil.rmtree(tempdir)
        assert result == expected

    def test_create_keyfile_directory_bad_permissions(self):
        tempdir = self.make_temp_directory()

        username = os.environ.get("USER")
        os.chmod(tempdir, dsa.KeyFileHandler.OCT755)
        kfh = dsa.KeyFileHandler(base_directory=tempdir)

        try:
            kfh.create_keyfile_directory(username)
        except AssemblyEnvironmentError:
            """This exception is the behaviour we want."""
        finally:
            shutil.rmtree(tempdir)

    def test_create_keyfile_directory_bad_location(self):
        tempdir = self.make_temp_directory()

        username = os.environ.get("USER")

        leaf = dsa.KeyFileHandler.default_leaf_directory
        leafdir = os.path.join(tempdir, username, leaf)
        fh = open(leafdir, "w")
        fh.write("somedata\n")
        fh.close()

        kfh = dsa.KeyFileHandler(base_directory=tempdir)

        try:
            kfh.create_keyfile_directory(username)
        except OSError as e:
            """This exception is the behaviour we want."""
            if not e.errno == FILE_EXISTS:
                raise
        finally:
            shutil.rmtree(tempdir)

    def test_create_keyfile_directory_bad_permissions2(self):
        tempdir = self.make_temp_directory()

        username = os.environ.get("USER")

        leaf = dsa.KeyFileHandler.default_leaf_directory
        leafdir = os.path.join(tempdir, username, leaf)
        # Disable write on directories to provoke exception
        os.mkdir(leafdir, dsa.KeyFileHandler.OCT500)
        os.chmod(leafdir, dsa.KeyFileHandler.OCT500)

        kfh = dsa.KeyFileHandler(base_directory=tempdir)

        try:
            kfh.create_keyfile_directory(username)
        except AssemblyEnvironmentError:
            """This exception is the behaviour we want."""
        finally:
            shutil.rmtree(tempdir)

    def test_create_keyfile_bad_permissions(self):
        tempdir = self.make_temp_directory()
        kfh = dsa.KeyFileHandler(base_directory=tempdir)

        username = os.environ.get("USER")
        kfh.create_keyfile_directory(username)

        test_bytes = "One Two Three Four".encode("utf-8")
        permissions = dsa.KeyFileHandler.OCT600
        filename = "assembly_elements_test_file"
        keypath = self._create_keyfile(kfh, username, filename, test_bytes, permissions)
        os.chmod(keypath, dsa.KeyFileHandler.OCT400)

        try:
            keypath = self._create_keyfile(
                kfh, username, filename, test_bytes, permissions
            )
        except IOError as e:
            """This exception is the behaviour we want."""
            if not e.errno == PERMISSION_DENIED:
                raise
        finally:
            shutil.rmtree(tempdir)

    def test_check_directory(self):
        tempdir = self.make_temp_directory()
        kfh = dsa.KeyFileHandler(base_directory=tempdir)

        username = os.environ.get("USER")
        keyfile_directory = kfh.create_keyfile_directory(username)
        permissions = dsa.KeyFileHandler.OCT700

        try:
            kfh.check_directory_permissions(keyfile_directory, username, permissions)
        finally:
            shutil.rmtree(tempdir)

    def test_check_directory_bad_permissions(self):
        pass

    def test_check_user_directory_permissions(self):
        tempdir = self.make_temp_directory()
        kfh = dsa.KeyFileHandler(base_directory=tempdir)

        username = os.environ.get("USER")
        userdir = os.path.join(tempdir, username)

        try:
            kfh.check_user_directory_permissions(userdir, username)
        finally:
            shutil.rmtree(tempdir)

    def test_check_keyfile_directory_permissions(self):
        tempdir = self.make_temp_directory()
        kfh = dsa.KeyFileHandler(base_directory=tempdir)

        username = os.environ.get("USER")

        keyfile_directory = kfh.create_keyfile_directory(username)

        try:
            kfh.check_keyfile_directory_permissions(keyfile_directory, username)
        finally:
            shutil.rmtree(tempdir)

    def test_check_directory_bad_owner(self):
        tempdir = self.make_temp_directory()
        kfh = dsa.KeyFileHandler(base_directory=tempdir)

        username = os.environ.get("USER")
        keyfile_directory = kfh.create_keyfile_directory(username)
        permissions = dsa.KeyFileHandler.OCT700
        bad_username = "root"

        try:
            kfh.check_directory_permissions(
                keyfile_directory, bad_username, permissions
            )
        except AssemblyRuntimeError:
            """This exception is the behaviour we want."""
        finally:
            shutil.rmtree(tempdir)

    def test_create_keyfile_bad_username(self):
        tempdir = self.make_temp_directory()
        kfh = dsa.KeyFileHandler(base_directory=tempdir)

        username = os.environ.get("USER")
        kfh.create_keyfile_directory(username)

        test_bytes = "One Two Three Four".encode("utf-8")
        permissions = dsa.KeyFileHandler.OCT600
        filename = "assembly_elements_test_file"
        keypath = self._create_keyfile(kfh, username, filename, test_bytes, permissions)

        try:
            bad_username = "root"
            kfh.create_keyfile(keypath, permissions, bad_username, keydata=test_bytes)
        except OSError as e:
            """This exception is the behaviour we want."""
            if not e.errno == NOT_PERMITTED:
                raise
        finally:
            shutil.rmtree(tempdir)

    def test_create_keyfile_directory_good_permissions(self):
        tempdir = self.make_temp_directory()

        username = os.environ.get("USER")

        os.chmod(tempdir, dsa.KeyFileHandler.OCT700)
        kfh = dsa.KeyFileHandler(base_directory=tempdir)

        try:
            kfh.create_keyfile_directory(username)
        finally:
            shutil.rmtree(tempdir)

    def test_create_keyfile_directory(self):
        tempdir = self.make_temp_directory()
        kfh = dsa.KeyFileHandler(base_directory=tempdir)

        username = os.environ.get("USER")
        try:
            kfh.create_keyfile_directory(username)
        except Exception:
            if os.path.exists(tempdir):
                shutil.rmtree(tempdir)
            raise

        if not os.path.exists(tempdir):
            raise Exception("dir {0} does not exist".format(tempdir))

        metadata = os.stat(tempdir)
        mode = stat.S_IMODE(metadata.st_mode)
        assert mode == dsa.KeyFileHandler.OCT700

        owner = metadata.st_uid
        user_id = pwd.getpwnam(username).pw_uid
        assert owner == user_id

        shutil.rmtree(tempdir)

    def test_get_keypath(self):
        tempdir = self.make_temp_directory()
        kfh = dsa.KeyFileHandler(base_directory=tempdir)

        username = os.environ.get("USER")

        filename = "assembly_elements_test_file_name"
        keypath = kfh.get_keypath(username, filename)
        assert keypath == os.path.join(tempdir, username, kfh.leaf_directory, filename)
        shutil.rmtree(tempdir)

    def test_get_keydata(self):
        tempdir = self.make_temp_directory()
        kfh = dsa.KeyFileHandler(base_directory=tempdir)

        username = os.environ.get("USER")
        kfh.create_keyfile_directory(username)

        test_bytes = "One Two Three Four".encode("utf-8")
        permissions = dsa.KeyFileHandler.OCT600
        filename = "assembly_elements_test_file"
        keypath = self._create_keyfile(kfh, username, filename, test_bytes, permissions)

        keydata = kfh.get_keydata(username, keypath)
        try:
            assert keydata == test_bytes
        except Exception:
            print("keydata: {} :: {}".format(type(keydata), keydata))
            print("test_bytes: {} :: {}".format(type(test_bytes), test_bytes))
            raise

        shutil.rmtree(tempdir)

    def _create_keyfile(self, kfh, username, filename, test_bytes, permissions):
        keypath = os.path.join(
            kfh.base_directory, username, kfh.leaf_directory, filename
        )

        kfh.create_keyfile(keypath, permissions, username, keydata=test_bytes)

        assert os.path.exists(keypath)
        return keypath

    def test_get_or_create_verifying_key(self):
        tempdir = self.make_temp_directory()
        kfh = dsa.KeyFileHandler(base_directory=tempdir)

        username = os.environ.get("USER")
        kfh.create_keyfile_directory(username)

        verifying_key = kfh.get_or_create_verifying_key(username)
        assert isinstance(verifying_key, ecdsa.VerifyingKey)

        shutil.rmtree(tempdir)

    def test_get_verifying_key(self):
        tempdir = self.make_temp_directory()
        kfh = dsa.KeyFileHandler(base_directory=tempdir)

        username = os.environ.get("USER")
        kfh.create_keyfile_directory(username)

        kfh.get_or_create_verifying_key(username)
        verifying_key = kfh.get_verifying_key(username)
        assert isinstance(verifying_key, ecdsa.VerifyingKey)

        shutil.rmtree(tempdir)

    def test_get_or_create_signing_key(self):
        tempdir = self.make_temp_directory()
        kfh = dsa.KeyFileHandler(base_directory=tempdir)

        username = os.environ.get("USER")
        kfh.create_keyfile_directory(username)

        signing_key = kfh.get_or_create_signing_key(username)
        assert isinstance(signing_key, ecdsa.SigningKey)

        shutil.rmtree(tempdir)

    def test_get_signing_key(self):
        tempdir = self.make_temp_directory()
        kfh = dsa.KeyFileHandler(base_directory=tempdir)

        username = os.environ.get("USER")
        kfh.create_keyfile_directory(username)

        kfh.get_or_create_signing_key(username)
        signing_key = kfh.get_signing_key(username)
        assert isinstance(signing_key, ecdsa.SigningKey)

        shutil.rmtree(tempdir)

    def test_create_keypair(self):
        tempdir = self.make_temp_directory()
        kfh = dsa.KeyFileHandler(base_directory=tempdir)

        username = os.environ.get("USER")
        kfh.create_keyfile_directory(username)

        kfh.create_keypair(username)
        signing_key1 = kfh.get_signing_key(username)
        assert isinstance(signing_key1, ecdsa.SigningKey)

        verifying_key1 = kfh.get_verifying_key(username)
        assert isinstance(verifying_key1, ecdsa.VerifyingKey)

        signing_key2 = kfh.get_or_create_signing_key(username)
        assert isinstance(signing_key2, ecdsa.SigningKey)

        verifying_key2 = kfh.get_or_create_verifying_key(username)
        assert isinstance(verifying_key2, ecdsa.VerifyingKey)

        assert signing_key1.to_string() == signing_key2.to_string()
        assert verifying_key1.to_string() == verifying_key2.to_string()

        shutil.rmtree(tempdir)

    def xtest_create_keyfile(self):
        username = os.environ.get("USER")
        tempdir = self.make_temp_directory()
        keypath = os.path.join(tempdir, "assembly_elements_test_file")
        permissions = dsa.KeyFileHandler.OCT600
        keydata = "X" * 1024
        dsa.KeyFileHandler.create_keyfile(keypath, permissions, username, keydata)
