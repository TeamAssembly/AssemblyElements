#!/usr/bin/env python

import os
import sys
import traceback

from twisted.application import internet, service
from twisted.web import server
from twisted.web import resource
from twisted import logger
from twisted import python

from assembly_elements import util

ASSEMBLY_ELEMENTS_BASEDIR = os.environ.get("ASSEMBLY_ELEMENTS_BASEDIR")

config_file = os.path.join(ASSEMBLY_ELEMENTS_BASEDIR, "config", "assembly_elements_simple.json")
config = util.config.ConfigRepo.get(config_file)

log_config = config.get("log")
observer = util.log.get_global_log_observer(log_config, **config.data)
port = config.get("web")["port"]

logger = logger.Logger()
logger.error("STARTING")


class Simple(resource.Resource):
    isLeaf = True

    def render_GET(self, request):
        return "<html>assembly_elements_simple.tac</html>"


appname = config.get("appname")
application = service.Application(appname)

site = server.Site(Simple())
sc = service.IServiceCollection(application)
i = internet.TCPServer(port, site)
i.setServiceParent(sc)
