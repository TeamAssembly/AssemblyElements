#!/usr/bin/env python

import os

from twisted.application import internet
from twisted.application import service
from twisted import logger

from assembly_elements import util
from assembly_elements.web import site
from assembly_elements.web.page import basic

ASSEMBLY_ELEMENTS_BASEDIR = os.environ.get("ASSEMBLY_ELEMENTS_BASEDIR")

config_file = os.path.join(ASSEMBLY_ELEMENTS_BASEDIR, "config", "assembly_elements.json")
config = util.config.ConfigRepo.get(config_file)

log_config = config.get("log")
log_observer = util.log.get_global_log_observer(log_config, **config.data)
port = config.get("web")["port"]

appname = config.get("appname")
application = service.Application(appname)
application.setComponent(logger.ILogObserver, log_observer)

site = site.AssemblySite(basic.Basic(port=port))
sc = service.IServiceCollection(application)
i = internet.TCPServer(port, site)
i.setServiceParent(sc)
