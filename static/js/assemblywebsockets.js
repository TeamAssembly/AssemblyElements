/*
 * Bidirectional RPC API implemented over websockets.
 */

function undef(value) {
    return typeof value === "undefined";
}

function AsssemblyWebSocketClient(protocol, hostname, port, path) {
    this.protocol = protocol;
    this.hostname = hostname;
    this.port = port;
    this.path = path;

    this.notification_type = -2;
    this.request_type = -1;
    this.response_type = 0;
    this.error_type = 1;

    if (this.port == "") {
        this.uri = this.protocol + "://" + this.hostname + this.path;
    } else {
        this.uri = this.protocol + "://" + this.hostname + ":" + this.port + this.path;
    }

    this.auto_reconnect = true;
    this.reconnect_backoff = [500,1000,1500,2500,4000,10000]
    this.reconnect_backoff_index = 0;
    this.default_timeout = 10000; // 10s timeout
    this.request_id = 0;
    this.request_queue = [];
    this.request_hash = {};
    this.registry = {}

    this.error_codes = {
        "no_such_method": 3,
        "unexpected_error": 2
    }

    // Pin this client to a global so we can access it in events
    if (undef(window.assembly_client)) {
        window.assembly_client = this;
    }
}

AsssemblyWebSocketClient.prototype.connect = function() {
    console.debug("AsssemblyWebSocketClient.connect");
    this.auto_reconnect = true;
    let socket = new WebSocket(this.uri);
    let self = this;
    socket.onclose = function(e) {
        self.onClose(e);
    };
    socket.onopen = function(e) {
        self.onOpen(e);
    };
    socket.onerror = function(e) {
        self.onError(e);
    };
    socket.onmessage = function(e) {
        self.onMessage(e);
    };
    this.socket = socket;
}

AsssemblyWebSocketClient.prototype.disconnect = function(auto_reconnect) {
    console.debug("AsssemblyWebSocketClient.disconnect");
    if (undef(auto_reconnect)) {
        auto_reconnect = false;
    }
    this.auto_reconnect = auto_reconnect;
    this.socket.close();
}

AsssemblyWebSocketClient.prototype.is_connected = function() {
    console.debug("AsssemblyWebSocketClient.is_connected");
    return this.socket.readyState === 1;
}

AsssemblyWebSocketClient.prototype.process_message = function(message) {
    console.debug("AsssemblyWebSocketClient.process_message");
    let message_type = message[0];
    if (message_type == this.notification_type) {
        let reply = false;
        return this.process_request(message, reply);
    } else if (message_type == this.request_type) {
        let reply = true;
        return this.process_request(message, reply);
    } else if (message_type == this.response_type || message_type == this.error_type) {
        return this.process_response(message);
    }
}

AsssemblyWebSocketClient.prototype.register_method = function(name, method) {
    this.registry[name] = method;
}

AsssemblyWebSocketClient.prototype.send_error_response = function(request_id, errno, message) {
    let envelope = [this.error_type, request_id, errno, message];
    let payload = JSON.stringify(envelope);
    this.send(payload);
}

AsssemblyWebSocketClient.prototype.send_success_response = function(request_id, result) {
    try {
        let envelope = [this.response_type, request_id, result];
        let payload = JSON.stringify(envelope);
        this.send(payload);
    } catch(e) {
        console.error("Failed to send success response to '"+this.uri+"': '"+String(e)+"'");
    }
}

AsssemblyWebSocketClient.prototype.process_request = function(message, reply) {
    // A peer has sent us a request which we must process

    let message_type = message[0];
    let request_id = message[1];
    let method_name = message[2];
    let args = message[3];
    let kwargs = message[4];

    let method = this.registry[method_name];
    if (undef(method)) {
        let errno = this.error_codes.no_such_method;
        let msg = "method '"+String(method_name)+"' does not exist or has not been exported"
        if (reply) {
            this.send_error_response(request_id, errno, msg);
        }
        return;
    }

    let result;
    try {
        result = method(args, kwargs);
        if (!reply) {
            return;
        }

        if (result instanceof Promise) {
            // fruity asynchronous mode
            let callback = function(real_result) {
                this.send_success_response(request_id, real_result);
            }
            let errback = function(real_result) {
                console.error("callback failed: '"+String(real_result)+"'");
                let errno = this.error_codes.unexpected_error;
                this.send_error_response(request_id, errno, real_result);
            }
            result.then(callback, errback);
        } else {
            // boring synchronous mode
            return this.send_success_response(request_id, result);
        }

    } catch(e) {
        if (!reply) {
            let errno = this.error_codes.unexpected_error;
            let msg = "an error occurred and your request could not be completed"
            this.send_error_response(request_id, errno, msg);
        }
    }

    // let envelope = [this.request_type, request_id, method, args, kwargs];

}

// Handle both responses and errors, depending on the 'message_type'
AsssemblyWebSocketClient.prototype.process_response = function(message) {
    console.debug("AsssemblyWebSocketClient.process_response");
    let message_type = message[0];
    let request_id = message[1];
    let result = message[2];
    if (undef(request_id) || undef(result)) {
        console.error("ERROR: Malformed response: '"+String(message)+"'");
        return;
    }
    let details = this.request_hash[request_id];
    if (undef(details)) {
        console.error("ERROR: No local request for request_id: '"+String(request_id)+"'");
        return;
    }
    let callback = details[0];
    let errback = details[1];
    let timeout_id = details[2];
    let submit_time = details[3];

    // Compute how long we waited on the response
    if (!undef(submit_time)) {
        try {
            var duration = Date.now() - submit_time;
            if (duration < 1000) {
                console.info("response in "+String(duration)+"ms");
            } else {
                console.info("response in "+String(duration/1000)+"s");
            }
        } catch(e) {
            console.error(e);
        }
    }

    // Try to clear the timeout for this request
    if (!undef(timeout_id)) {
        try {
            window.clearTimeout(timeout_id);
        } catch(e) {
            console.error("ERROR: Failed to clearTimeout: " + String(e))
        }
    }

    // Try the callback (if this is a 'response_type' message
    if (message_type == this.response_type) {
        try {
            return callback(result);
        } catch(e) {
            console.error(e);
            console.error("ERROR: Failed to call callback: '"+String(callback)+"' :: '"+String(result)+"'");
        }
    }

    // Call the errback
    try {
        return errback(result);
    } catch(e) {
        console.error("ERROR: Failed to call errback: '"+String(errback)+"' :: '"+String(result)+"'");
    }
}

AsssemblyWebSocketClient.prototype.onOpen = function(e) {
    console.debug("AsssemblyWebSocketClient.onOpen");
    this.reconnect_backoff_index = 0;

    let payload;

    while (this.request_queue.length > 0) {
        payload = this.request_queue.shift();
        this.send(payload);
    }
}

AsssemblyWebSocketClient.prototype.onMessage = function(e) {
    console.debug("AsssemblyWebSocketClient.onMessage");
    let message;
    try {
        message = JSON.parse(e.data);
        try {
            return this.process_message(message);
        } catch(ex) {
            console.error("ERROR: Malformed response: '"+String(e.data)+"' :: '"+String(ex)+"'");
        }
    } catch(ex) {
        console.error("ERROR: Malformed response: '"+String(e.data)+"' :: '"+String(ex)+"'");
    }
}

AsssemblyWebSocketClient.prototype.onClose = function(e) {
    console.debug("AsssemblyWebSocketClient.onClose");
    console.debug(this.auto_reconnect);
    console.debug(e);
    console.debug(this);
    console.debug(typeof this);
    let self = this;
    if (this.auto_reconnect === false) {
        this.cleanup();
    } else {
        // Try to reconnect after 'backoff';
        let backoff = this.reconnect_backoff[this.reconnect_backoff_index];
        this.reconnect_backoff_index = Math.min(this.reconnect_backoff_index+1, this.reconnect_backoff.length-1);
        window.setTimeout(function() {
            console.info("timeout complete - attempting reconnect");
            self.connect();
        }, 2000);
    }
}

AsssemblyWebSocketClient.prototype.get_backoff = function(e) {
    let backoff = this.reconnect_backoff[this.reconnect_backoff_index];
    this.reconnect_backoff_index = Math.min(this.reconnect_backoff_index+1, this.reconnect_backoff.length-1);
    return backoff;
}

AsssemblyWebSocketClient.prototype.onError = function(e) {
    console.debug("AsssemblyWebSocketClient.onError");
    console.error(e);
    /*
     * Cleanup, but auto-reconnect
     */
    this.cleanup(e.data);
    let auto_reconnect = true;
    this.disconnect(auto_reconnect);
}

AsssemblyWebSocketClient.prototype.cleanup = function(reason) {
    /*
     * For any pending request:
     *  - call its callback
     *  - cancel its timeout
     *  - delete it from this.request_hash
     */
    console.debug("AsssemblyWebSocketClient.cleanup");
    let request_id;
    let callbacks;
    let errback;
    let timeout_id;
    let request_ids = Object.keys(this.request_hash);
    for (let i=0; i < request_ids.length; i++) {
        request_id = request_ids[i];
        try {
            callbacks = this.request_hash[request_id];
            delete this.request_hash[request_id];
            errback = callbacks[1];
            timeout_id = callbacks[2];
            errback("ERROR: Connection failed: " + String(reason));
            window.clearTimeout(timeout_id);
        } catch(e) {
            if (request_id === null) continue;
            console.error("ERROR: Failed to cancel '"+String(request_id)+"': '"+String(e)+"'");
        }
    }
}

AsssemblyWebSocketClient.prototype.call = function(method, args, kwargs, callback, errback, timeout) {
    console.debug("AsssemblyWebSocketClient.call: '"+String(method)+"'")
    if (undef(args) || args === null) {
        args = [];
    }

    if (undef(kwargs) || kwargs === null) {
        kwargs = {};
    }

    if (undef(timeout)) {
        timeout = this.default_timeout;
    }

    if (undef(callback)) {
        callback = this.default_callback;
    }

    if (undef(errback)) {
        errback = this.default_errback;
    }

    let request_id = this.request_id;
    this.request_id++;

    let envelope = [this.request_type, request_id, method, args, kwargs];
    let payload = JSON.stringify(envelope);
    this.send(payload);

    let self = this;
    let timeout_id = window.setTimeout(self.trigger_timeout, timeout, request_id);

    this.request_hash[request_id] = [callback, errback, timeout_id, Date.now()];
    return request_id;
}

AsssemblyWebSocketClient.prototype.default_callback = function(data) {
    console.debug("AsssemblyWebSocketClient.default_callback: '"+String(data)+"'")
}

AsssemblyWebSocketClient.prototype.default_errback = function(data) {
    console.error("AsssemblyWebSocketClient.default_errback: '"+String(data)+"'")
}

AsssemblyWebSocketClient.prototype.trigger_timeout = function(request_id) {
    console.debug("AsssemblyWebSocketClient.trigger_timeout: '"+String(request_id)+"'")

    let details = this.request_hash[request_id];
    let callback = details[0];
    let errback = details[1];
    let timeout_id = details[2];
    let submit_time = details[3];

    // Compute how long we waited on the response
    if (!undef(submit_time)) {
        try {
            var duration = Date.now() - submit_time;
            console.info("response in '"+String(duration)+"'");
        } catch(e) {
            console.error(e);
        }
    }

    // Compute a 'result' string
    let result = "timeout after 'duration': " + String(duration);

    // Call the request from the hash
    try {
        delete this.request_hash[request_id];
    } catch(e) {
        console.error(e);
    }

    // Call the errback
    try {
        return errback(result);
    } catch(e) {
        console.error("ERROR: Failed to call errback: '"+String(errback)+"' :: '"+String(result)+"'");
    }
}

AsssemblyWebSocketClient.prototype.send = function(payload) {
    console.debug("AsssemblyWebSocketClient.send");
    if (this.is_connected()) {
        return this.socket.send(payload);
    } else {
        this.request_queue.push(payload);
    }
}
