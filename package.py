# -*- coding: utf-8 -*-

name = 'assembly_elements'

version = '0.7.0'

authors = ['Donal McMullan']

variants = [
]

def commands():
    env.PATH.prepend("{root}/AssemblyElements/bin")
    env.PYTHONPATH.prepend("{root}/AssemblyElements/lib/python")
    env.ASSEMBLY_ELEMENTS_BASEDIR.set("{root}/AssemblyElements")
