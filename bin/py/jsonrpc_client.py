#!/usr/bin/env python

from assembly_elements.web.jsonrpc import mixin

from autobahn.twisted.websocket import WebSocketClientProtocol
from autobahn.twisted.websocket import WebSocketClientFactory


class MyClientProtocol(mixin.JsonRpcProtocolMixin, WebSocketClientProtocol):

    def onConnect(self, response):
        print("Server connected: {0}".format(response.peer))

    def onConnecting(self, transport_details):
        print("Connecting; transport details: {}".format(transport_details))
        return None  # ask for defaults



if __name__ == '__main__':


    import logging
    from twisted import logger
    from twisted.internet import reactor

    log = logger.Logger()
    logging.basicConfig(level=logging.DEBUG)
    logger.globalLogPublisher.addObserver(logger.STDLibLogObserver())

    factory = WebSocketClientFactory(u"ws://127.0.0.1:8092/jsonrpcws/")
    factory.protocol = MyClientProtocol

    reactor.connectTCP("127.0.0.1", 8092, factory)
    reactor.run()

