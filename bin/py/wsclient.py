#!/usr/bin/env python

from twisted.internet import task
from twisted.internet import defer

from assembly_elements.web import client

from autobahn.twisted.websocket import WebSocketClientProtocol
from autobahn.twisted.websocket import WebSocketClientFactory


def main(reactor):
    completion = defer.Deferred()

    factory = client.AssemblyWebSocketClientFactory(u"ws://127.0.0.1:8092/ws/")
    reactor.connectTCP("127.0.0.1", 8092, factory)

    return completion


task.react(main)
