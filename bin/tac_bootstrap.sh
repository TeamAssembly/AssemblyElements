#!/bin/zsh

scriptname=`basename $0` # assembly_elements

script_username="donal"
script_groupname="donal"

HOSTNAME=`hostname`

bindir=$(cd -P -- "$(dirname -- "$0")" && printf '%s\n' "$(pwd -P)")
export ASSEMBLY_ELEMENTS_BASEDIR=`dirname $bindir`
export APPNAME=$scriptname

pydir=""
if [ -e "${ASSEMBLY_ELEMENTS_BASEDIR}/assembly_elements/__init__.py" ]; then
   pydir="${ASSEMBLY_ELEMENTS_BASEDIR}"
elif [ -e "${ASSEMBLY_ELEMENTS_BASEDIR}/lib/python/assembly_elements/__init__.py" ]; then
   pydir="${ASSEMBLY_ELEMENTS_BASEDIR}/lib/python"
elif [ -e "${ASSEMBLY_ELEMENTS_BASEDIR}/src/assembly_elements/__init__.py" ]; then
   pydir="${ASSEMBLY_ELEMENTS_BASEDIR}/src/"
fi

if [ ! -z "${pydir}" ]; then
    export PYTHONPATH="${pydir}:${PYTHONPATH}"
fi

if [  -z "${VIRTUAL_ENV}" ]; then
    if [ -d "${ASSEMBLY_ELEMENTS_BASEDIR}/venv" ]; then
        source "${ASSEMBLY_ELEMENTS_BASEDIR}/venv/bin/activate"
    fi
fi

if [ -e $VIRTUAL_ENV ]; then
    INTERPRETER=`which python`
    INTERPRETER=`python -c "import os; print(os.path.realpath('"$INTERPRETER"'))"`
    INTERPRETER=`basename $INTERPRETER`
fi

id $script_username > /dev/null
if [ ! $? -eq 0 ]; then
    echo "ERROR: UNIX USER '$script_username' DOES NOT EXIST"
    exit 1
fi

OS=`uname`
python -c 'import grp; print(grp.getgrnam("'$script_groupname'").gr_gid)' > /dev/null 2> /dev/null
if [ ! $? -eq 0 ]; then
    echo "ERROR: UNIX GROUP '$script_groupname' DOES NOT EXIST"
    exit 1
fi

if [ "$USER" != "$script_username" ] && [ "$USER" != "root" ]
then
    echo "ERROR: This process can only be managed by $script_username or root. Not ${USER}"
    exit 1
fi

tmp_directory="/tmp/${APPNAME}"
if [ ! -d $tmp_directory ]; then
    mkdir -p $tmp_directory
    chown $script_username:$script_groupname $tmp_directory
    chmod 750 $tmp_directory
fi

cd $tmp_directory 2> /dev/null
if [ ! $? -eq 0 ]; then
    echo "ERROR: FAILED TO cd INTO TMP DIRECTORY ${tmp_directory}"
    echo "PLEASE FIX PERMISSIONS"
    exit 1
fi

pidfile_directory="/var/run/${APPNAME}"
if [ ! -d $pidfile_directory ]; then
    mkdir -p $pidfile_directory 2> /dev/null
    if [ ! $? -eq 0 ]; then
        echo "ERROR: PID DIRECTORY '${pidfile_directory}' DOES NOT EXIST"
        if [ ! "$USER" = "root" ]; then
            echo "AND YOU DO NOT HAVE PERMISSION TO CREATE IT"
        fi
        exit 1
    fi
    chown $script_username:$script_username $pidfile_directory
    chmod 750 $pidfile_directory
fi

log_directory="/var/log/${APPNAME}"
if [ ! -d $log_directory ]; then
    mkdir -p $log_directory 2> /dev/null
    if [ ! $? -eq 0 ]; then
        echo "ERROR: LOG DIRECTORY '${log_directory}' DOES NOT EXIST"
        if [ ! "$USER" = "root" ]; then
            echo "AND YOU DO NOT HAVE PERMISSION TO CREATE IT"
        fi
        exit 1
    fi
    chown $script_username:$script_username $log_directory
    chmod 750 $log_directory
fi

pidfile="${pidfile_directory}/${APPNAME}.pid"

script_uid="$(/usr/bin/id -u $script_username)"
script_gid=`python -c 'import grp; print(grp.getgrnam("'$script_groupname'").gr_gid)'`

SHUTDOWN=0
STARTUP=0

if [ "$1" = "start" ]; then
    STARTUP=1
elif [ "$1" = "restart" ]; then
    STARTUP=1
    SHUTDOWN=1
elif [ "$1" = "stop" ]; then
    SHUTDOWN=1
elif [ "$1" = "status" ]; then

    if [ ! -e $pidfile ]; then
        echo "${APPNAME} IS NOT RUNNING"
        exit 0
    fi
    pid=$(cat $pidfile)
    if [ "${OS}" = "Darwin" ]; then
        pid_process="$(ps -hp $pid -o comm=)"
    else
        pid_process="$(ps --no-headers -p $pid -o comm=)"
    fi

    if [ -z "${pid_process}" ]; then
        echo "${APPNAME} IS NOT RUNNING"
        echo "PIDFILE AT ${pidfile} FOR PID ${pid} IS STALE"
    elif [[ "$INTERPRETER" = "$(basename $pid_process)" ]]; then
        echo "${APPNAME} IS RUNNING WITH PID ${pid}"
    else
        echo "${APPNAME} IS NOT RUNNING"
        echo "PIDFILE AT ${pidfile} FOR PID ${pid} IS STALE"
        echo "PROCESS WITH PID ${pid} IS ${pid_process}"
    fi
    exit 0
fi;

if [ "$SHUTDOWN" = "1" ]; then

    if [ ! -e $pidfile ]; then
        echo "${APPNAME} IS NOT RUNNING"
    else
        pid=$(cat $pidfile)
        if [ "$pid" != "" ]; then
            if [ "${OS}" = "Darwin" ]; then
                pid_process="$(ps -hp $pid -o comm=)"
            else
                pid_process="$(ps --no-headers -p $pid -o comm=)"
            fi

            process_base_name="$(basename $pid_process)"
            if [[ "$INTERPRETER" = $process_base_name ]]; then
                kill $pid
            fi;

            if [ "${OS}" = "Darwin" ]; then
                pid_process="$(ps -hp $pid -o comm=)"
            else
                pid_process="$(ps --no-headers -p $pid -o comm=)"
            fi
            if [ ! -z "${pid_process}" ]; then
                while [[ "$INTERPRETER" = "$(basename $pid_process)" ]]; do
                    echo "WAITING FOR PID $pid"
                    sleep 1
                    if [ "${OS}" = "Darwin" ]; then
                        pid_process="$(ps -hp $pid -o comm=)"
                    else
                        pid_process="$(ps --no-headers -p $pid -o comm=)"
                    fi
                    if [ -z "${pid_process}" ]; then
                        break
                    fi
                done
            fi
        fi
    fi
fi;

interpreter=`which $INTERPRETER`
if [ $? -eq 0 ]; then
    # Ok - $INTERPRETER is in PATH
else
    echo "COULD NOT FIND $INTERPRETER"
    exit 1
fi


twistd=`which twistd`
if [ $? -eq 0 ]; then
    # Ok - twistd is in PATH
else
    echo "COULD NOT FIND twistd"
    exit 1
fi

export PYTHONDONTWRITEBYTECODE=1

# Get the uid of the user who's running this script
euid=$(id -u)

if [ "$STARTUP" = "1" ]; then
    if [ "$script_username" = "root" ]; then
        exec $interpreter $twistd                   --gid $script_gid --pidfile ${pidfile} -oy $ASSEMBLY_ELEMENTS_BASEDIR/tac/$scriptname.tac
    elif [ "${euid}" = "${script_uid}" ]; then
        exec $interpreter $twistd                   --gid $script_gid --pidfile ${pidfile} -oy $ASSEMBLY_ELEMENTS_BASEDIR/tac/$scriptname.tac
    else
        exec $interpreter $twistd --uid $script_uid --gid $script_gid --pidfile ${pidfile} -oy $ASSEMBLY_ELEMENTS_BASEDIR/tac/$scriptname.tac
    fi
fi;
