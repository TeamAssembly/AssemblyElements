#!/bin/zsh

scriptname=`basename $0` # assembly_elements

bindir=$(cd -P -- "$(dirname -- "$0")" && printf '%s\n' "$(pwd -P)")
export BASEDIR=`dirname $bindir`
pydir="${BASEDIR}/lib/python"

if [  -z "${VIRTUAL_ENV}" ]; then
    if [ -d "${BASEDIR}/venv" ]; then
        source "${BASEDIR}/venv/bin/activate"
    fi
fi

if [  -z "${VIRTUAL_ENV}" ]; then
    if [ -d "${BASEDIR}/venv_pypy_3" ]; then
        source "${BASEDIR}/venv_pypy_3/bin/activate"
    fi
fi

if [  -z "${VIRTUAL_ENV}" ]; then
    if [ -d "${BASEDIR}/venv_python_3" ]; then
        source "${BASEDIR}/venv_python_3/bin/activate"
    fi
fi

if [ ! -z "${pydir}" ]; then
    export PYTHONPATH="${pydir}:${PYTHONPATH}"
fi

exec python "${bindir}/py/${scriptname}.py" "$@"
